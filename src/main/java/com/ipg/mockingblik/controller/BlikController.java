package com.ipg.mockingblik.controller;

import com.ipg.mockingblik.response.DirectResponseSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@Controller
public class BlikController {
    private final DirectResponseSender responseSender;

    @Autowired
    public BlikController(DirectResponseSender responseSender) {
        this.responseSender = responseSender;
    }

    @GetMapping
    public String getIndex() {
        return "index";
    }

    @GetMapping(value = "/mockrequest")
    public String mockRequest() {
        return "mockrequest";
    }

    @PostMapping(value = "/tx", consumes = "application/x-www-form-urlencoded")
    public String processPurchaseRequest(Model model, @RequestBody MultiValueMap<String, String> params) {
        model.addAttribute("entries", params.toSingleValueMap().entrySet());
        return "transaction";
    }

    @PostMapping(value = "/response", consumes = "application/x-www-form-urlencoded")
    public String processResponse(@RequestBody MultiValueMap<String, String> params) {
        Map<String, String> paramsMap = params.toSingleValueMap();

        if (paramsMap.get("responseType").equalsIgnoreCase("REST")) {
            responseSender.sendRestResponse(paramsMap);
        } else {
            responseSender.sensSoapResponse(paramsMap);
        }

        return "success";
    }
}