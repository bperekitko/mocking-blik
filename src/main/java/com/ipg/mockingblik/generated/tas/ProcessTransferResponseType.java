
package com.ipg.mockingblik.generated.tas;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for processTransferResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="processTransferResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}ResponseType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="transfer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
 *                   &lt;element name="appURL" type="{http://www.hp.com/mobicore/xfmf/common}UrlType" minOccurs="0"/>
 *                   &lt;element name="alias" type="{http://www.hp.com/mobicore/xfmf/common}AliasMobpayType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="alias" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}AliasMobpayType">
 *                 &lt;sequence>
 *                   &lt;element name="alternatives">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence maxOccurs="10">
 *                             &lt;element name="alternative" type="{http://www.hp.com/mobicore/xfmf/common}AlternativeType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "processTransferResponseType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "transfer",
    "alias"
})
public class ProcessTransferResponseType
    extends ResponseType
{

    protected ProcessTransferResponseType.Transfer transfer;
    protected ProcessTransferResponseType.Alias alias;

    /**
     * Gets the value of the transfer property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessTransferResponseType.Transfer }
     *     
     */
    public ProcessTransferResponseType.Transfer getTransfer() {
        return transfer;
    }

    /**
     * Sets the value of the transfer property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessTransferResponseType.Transfer }
     *     
     */
    public void setTransfer(ProcessTransferResponseType.Transfer value) {
        this.transfer = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessTransferResponseType.Alias }
     *     
     */
    public ProcessTransferResponseType.Alias getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessTransferResponseType.Alias }
     *     
     */
    public void setAlias(ProcessTransferResponseType.Alias value) {
        this.alias = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}AliasMobpayType">
     *       &lt;sequence>
     *         &lt;element name="alternatives">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence maxOccurs="10">
     *                   &lt;element name="alternative" type="{http://www.hp.com/mobicore/xfmf/common}AlternativeType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "alternatives"
    })
    public static class Alias
        extends AliasMobpayType
    {

        @XmlElement(required = true)
        protected ProcessTransferResponseType.Alias.Alternatives alternatives;

        /**
         * Gets the value of the alternatives property.
         * 
         * @return
         *     possible object is
         *     {@link ProcessTransferResponseType.Alias.Alternatives }
         *     
         */
        public ProcessTransferResponseType.Alias.Alternatives getAlternatives() {
            return alternatives;
        }

        /**
         * Sets the value of the alternatives property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessTransferResponseType.Alias.Alternatives }
         *     
         */
        public void setAlternatives(ProcessTransferResponseType.Alias.Alternatives value) {
            this.alternatives = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence maxOccurs="10">
         *         &lt;element name="alternative" type="{http://www.hp.com/mobicore/xfmf/common}AlternativeType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "alternative"
        })
        public static class Alternatives {

            @XmlElement(required = true)
            protected List<AlternativeType> alternative;

            /**
             * Gets the value of the alternative property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the alternative property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAlternative().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link AlternativeType }
             * 
             * 
             */
            public List<AlternativeType> getAlternative() {
                if (alternative == null) {
                    alternative = new ArrayList<AlternativeType>();
                }
                return this.alternative;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
     *         &lt;element name="appURL" type="{http://www.hp.com/mobicore/xfmf/common}UrlType" minOccurs="0"/>
     *         &lt;element name="alias" type="{http://www.hp.com/mobicore/xfmf/common}AliasMobpayType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "txRef",
        "appURL",
        "alias"
    })
    public static class Transfer {

        protected long txRef;
        protected String appURL;
        protected AliasMobpayType alias;

        /**
         * Gets the value of the txRef property.
         * 
         */
        public long getTxRef() {
            return txRef;
        }

        /**
         * Sets the value of the txRef property.
         * 
         */
        public void setTxRef(long value) {
            this.txRef = value;
        }

        /**
         * Gets the value of the appURL property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAppURL() {
            return appURL;
        }

        /**
         * Sets the value of the appURL property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAppURL(String value) {
            this.appURL = value;
        }

        /**
         * Gets the value of the alias property.
         * 
         * @return
         *     possible object is
         *     {@link AliasMobpayType }
         *     
         */
        public AliasMobpayType getAlias() {
            return alias;
        }

        /**
         * Sets the value of the alias property.
         * 
         * @param value
         *     allowed object is
         *     {@link AliasMobpayType }
         *     
         */
        public void setAlias(AliasMobpayType value) {
            this.alias = value;
        }

    }

}
