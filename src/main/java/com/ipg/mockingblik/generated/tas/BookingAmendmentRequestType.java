
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Serwis do korekt transakcji w systemie BackOffice. Serwis synchroniczny
 * 
 * <p>Java class for bookingAmendmentRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="bookingAmendmentRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="acquirer">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="extTxRef" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="orgTxRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
 *                   &lt;/sequence>
 *                   &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="tas">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
 *                     &lt;element name="orgExtTxRef" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *         &lt;element name="txData">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="type" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;maxLength value="15"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="txDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *                 &lt;attribute name="locDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *                 &lt;attribute name="rrn" type="{http://www.hp.com/mobicore/xfmf/common}RrnType" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="finData">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}FinDataType">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="stlData" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}MoneyType">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="couponCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="adviceFlg" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bookingAmendmentRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "acquirer",
    "tas",
    "txData",
    "finData",
    "stlData",
    "couponCode",
    "adviceFlg"
})
public class BookingAmendmentRequestType
    extends RequestType
{

    protected BookingAmendmentRequestType.Acquirer acquirer;
    protected BookingAmendmentRequestType.Tas tas;
    @XmlElement(required = true)
    protected BookingAmendmentRequestType.TxData txData;
    @XmlElement(required = true)
    protected BookingAmendmentRequestType.FinData finData;
    protected BookingAmendmentRequestType.StlData stlData;
    protected String couponCode;
    @XmlElement(defaultValue = "false")
    protected Boolean adviceFlg;

    /**
     * Gets the value of the acquirer property.
     * 
     * @return
     *     possible object is
     *     {@link BookingAmendmentRequestType.Acquirer }
     *     
     */
    public BookingAmendmentRequestType.Acquirer getAcquirer() {
        return acquirer;
    }

    /**
     * Sets the value of the acquirer property.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingAmendmentRequestType.Acquirer }
     *     
     */
    public void setAcquirer(BookingAmendmentRequestType.Acquirer value) {
        this.acquirer = value;
    }

    /**
     * Gets the value of the tas property.
     * 
     * @return
     *     possible object is
     *     {@link BookingAmendmentRequestType.Tas }
     *     
     */
    public BookingAmendmentRequestType.Tas getTas() {
        return tas;
    }

    /**
     * Sets the value of the tas property.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingAmendmentRequestType.Tas }
     *     
     */
    public void setTas(BookingAmendmentRequestType.Tas value) {
        this.tas = value;
    }

    /**
     * Gets the value of the txData property.
     * 
     * @return
     *     possible object is
     *     {@link BookingAmendmentRequestType.TxData }
     *     
     */
    public BookingAmendmentRequestType.TxData getTxData() {
        return txData;
    }

    /**
     * Sets the value of the txData property.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingAmendmentRequestType.TxData }
     *     
     */
    public void setTxData(BookingAmendmentRequestType.TxData value) {
        this.txData = value;
    }

    /**
     * Gets the value of the finData property.
     * 
     * @return
     *     possible object is
     *     {@link BookingAmendmentRequestType.FinData }
     *     
     */
    public BookingAmendmentRequestType.FinData getFinData() {
        return finData;
    }

    /**
     * Sets the value of the finData property.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingAmendmentRequestType.FinData }
     *     
     */
    public void setFinData(BookingAmendmentRequestType.FinData value) {
        this.finData = value;
    }

    /**
     * Gets the value of the stlData property.
     * 
     * @return
     *     possible object is
     *     {@link BookingAmendmentRequestType.StlData }
     *     
     */
    public BookingAmendmentRequestType.StlData getStlData() {
        return stlData;
    }

    /**
     * Sets the value of the stlData property.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingAmendmentRequestType.StlData }
     *     
     */
    public void setStlData(BookingAmendmentRequestType.StlData value) {
        this.stlData = value;
    }

    /**
     * Gets the value of the couponCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCouponCode() {
        return couponCode;
    }

    /**
     * Sets the value of the couponCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCouponCode(String value) {
        this.couponCode = value;
    }

    /**
     * Gets the value of the adviceFlg property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAdviceFlg() {
        return adviceFlg;
    }

    /**
     * Sets the value of the adviceFlg property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAdviceFlg(Boolean value) {
        this.adviceFlg = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="extTxRef" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="orgTxRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
     *       &lt;/sequence>
     *       &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "extTxRef",
        "orgTxRef"
    })
    public static class Acquirer {

        @XmlElement(required = true)
        protected String extTxRef;
        protected long orgTxRef;
        @XmlAttribute(name = "acqId", required = true)
        protected long acqId;

        /**
         * Gets the value of the extTxRef property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExtTxRef() {
            return extTxRef;
        }

        /**
         * Sets the value of the extTxRef property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExtTxRef(String value) {
            this.extTxRef = value;
        }

        /**
         * Gets the value of the orgTxRef property.
         * 
         */
        public long getOrgTxRef() {
            return orgTxRef;
        }

        /**
         * Sets the value of the orgTxRef property.
         * 
         */
        public void setOrgTxRef(long value) {
            this.orgTxRef = value;
        }

        /**
         * Gets the value of the acqId property.
         * 
         */
        public long getAcqId() {
            return acqId;
        }

        /**
         * Sets the value of the acqId property.
         * 
         */
        public void setAcqId(long value) {
            this.acqId = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}FinDataType">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class FinData
        extends FinDataType
    {


    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}MoneyType">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class StlData
        extends MoneyType
    {


    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
     *         &lt;element name="orgExtTxRef" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "txRef",
        "orgExtTxRef"
    })
    public static class Tas {

        protected long txRef;
        @XmlElement(required = true)
        protected String orgExtTxRef;

        /**
         * Gets the value of the txRef property.
         * 
         */
        public long getTxRef() {
            return txRef;
        }

        /**
         * Sets the value of the txRef property.
         * 
         */
        public void setTxRef(long value) {
            this.txRef = value;
        }

        /**
         * Gets the value of the orgExtTxRef property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrgExtTxRef() {
            return orgExtTxRef;
        }

        /**
         * Sets the value of the orgExtTxRef property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrgExtTxRef(String value) {
            this.orgExtTxRef = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="type" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;maxLength value="15"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="txDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
     *       &lt;attribute name="locDate" use="required" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
     *       &lt;attribute name="rrn" type="{http://www.hp.com/mobicore/xfmf/common}RrnType" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TxData {

        @XmlAttribute(name = "type", required = true)
        protected String type;
        @XmlAttribute(name = "txDate")
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar txDate;
        @XmlAttribute(name = "locDate", required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar locDate;
        @XmlAttribute(name = "rrn")
        protected String rrn;

        /**
         * Gets the value of the type property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Sets the value of the type property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

        /**
         * Gets the value of the txDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getTxDate() {
            return txDate;
        }

        /**
         * Sets the value of the txDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setTxDate(XMLGregorianCalendar value) {
            this.txDate = value;
        }

        /**
         * Gets the value of the locDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getLocDate() {
            return locDate;
        }

        /**
         * Sets the value of the locDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setLocDate(XMLGregorianCalendar value) {
            this.locDate = value;
        }

        /**
         * Gets the value of the rrn property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRrn() {
            return rrn;
        }

        /**
         * Sets the value of the rrn property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRrn(String value) {
            this.rrn = value;
        }

    }

}
