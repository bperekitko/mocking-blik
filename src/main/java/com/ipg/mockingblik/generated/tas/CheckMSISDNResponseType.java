
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Synchroniczna odpowiedź.
 * 
 * <p>Java class for checkMSISDNResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="checkMSISDNResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}ResponseType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="msisdnStatus" type="{http://www.hp.com/mobicore/xfmf/common}MSISDNStatusType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "checkMSISDNResponseType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "msisdnStatus"
})
public class CheckMSISDNResponseType
    extends ResponseType
{

    @XmlSchemaType(name = "string")
    protected MSISDNStatusType msisdnStatus;

    /**
     * Gets the value of the msisdnStatus property.
     * 
     * @return
     *     possible object is
     *     {@link MSISDNStatusType }
     *     
     */
    public MSISDNStatusType getMsisdnStatus() {
        return msisdnStatus;
    }

    /**
     * Sets the value of the msisdnStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link MSISDNStatusType }
     *     
     */
    public void setMsisdnStatus(MSISDNStatusType value) {
        this.msisdnStatus = value;
    }

}
