
package com.ipg.mockingblik.generated.tas;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getAppDataResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAppDataResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}ResponseType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="alias" type="{http://www.hp.com/mobicore/xfmf/common}AliasType"/>
 *         &lt;element name="paramAliasList">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence maxOccurs="unbounded">
 *                   &lt;element name="paramAlias" type="{http://www.hp.com/mobicore/xfmf/common}AliasParamType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAppDataResponseType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "alias",
    "paramAliasList"
})
public class GetAppDataResponseType
    extends ResponseType
{

    protected AliasType alias;
    protected GetAppDataResponseType.ParamAliasList paramAliasList;

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link AliasType }
     *     
     */
    public AliasType getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link AliasType }
     *     
     */
    public void setAlias(AliasType value) {
        this.alias = value;
    }

    /**
     * Gets the value of the paramAliasList property.
     * 
     * @return
     *     possible object is
     *     {@link GetAppDataResponseType.ParamAliasList }
     *     
     */
    public GetAppDataResponseType.ParamAliasList getParamAliasList() {
        return paramAliasList;
    }

    /**
     * Sets the value of the paramAliasList property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetAppDataResponseType.ParamAliasList }
     *     
     */
    public void setParamAliasList(GetAppDataResponseType.ParamAliasList value) {
        this.paramAliasList = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence maxOccurs="unbounded">
     *         &lt;element name="paramAlias" type="{http://www.hp.com/mobicore/xfmf/common}AliasParamType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "paramAlias"
    })
    public static class ParamAliasList {

        @XmlElement(required = true)
        protected List<AliasParamType> paramAlias;

        /**
         * Gets the value of the paramAlias property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the paramAlias property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getParamAlias().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AliasParamType }
         * 
         * 
         */
        public List<AliasParamType> getParamAlias() {
            if (paramAlias == null) {
                paramAlias = new ArrayList<AliasParamType>();
            }
            return this.paramAlias;
        }

    }

}
