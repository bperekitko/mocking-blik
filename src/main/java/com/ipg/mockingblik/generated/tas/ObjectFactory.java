
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ipg.mockingblik.generated.tas package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ipg.mockingblik.generated.tas
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AdditionalInfoListType }
     * 
     */
    public AdditionalInfoListType createAdditionalInfoListType() {
        return new AdditionalInfoListType();
    }

    /**
     * Create an instance of {@link SimpleTransferType }
     * 
     */
    public SimpleTransferType createSimpleTransferType() {
        return new SimpleTransferType();
    }

    /**
     * Create an instance of {@link ErrorStatusType }
     * 
     */
    public ErrorStatusType createErrorStatusType() {
        return new ErrorStatusType();
    }

    /**
     * Create an instance of {@link SimpleTransactionType }
     * 
     */
    public SimpleTransactionType createSimpleTransactionType() {
        return new SimpleTransactionType();
    }

    /**
     * Create an instance of {@link MerchantTransferDetailsType }
     * 
     */
    public MerchantTransferDetailsType createMerchantTransferDetailsType() {
        return new MerchantTransferDetailsType();
    }

    /**
     * Create an instance of {@link EventType }
     * 
     */
    public EventType createEventType() {
        return new EventType();
    }

    /**
     * Create an instance of {@link EventType.Data }
     * 
     */
    public EventType.Data createEventTypeData() {
        return new EventType.Data();
    }

    /**
     * Create an instance of {@link EventType.Data.Ctx }
     * 
     */
    public EventType.Data.Ctx createEventTypeDataCtx() {
        return new EventType.Data.Ctx();
    }

    /**
     * Create an instance of {@link EventType.Data.Key }
     * 
     */
    public EventType.Data.Key createEventTypeDataKey() {
        return new EventType.Data.Key();
    }

    /**
     * Create an instance of {@link MerchantDetailsType }
     * 
     */
    public MerchantDetailsType createMerchantDetailsType() {
        return new MerchantDetailsType();
    }

    /**
     * Create an instance of {@link PagingType }
     * 
     */
    public PagingType createPagingType() {
        return new PagingType();
    }

    /**
     * Create an instance of {@link ParametersListType }
     * 
     */
    public ParametersListType createParametersListType() {
        return new ParametersListType();
    }

    /**
     * Create an instance of {@link ListResponseType }
     * 
     */
    public ListResponseType createListResponseType() {
        return new ListResponseType();
    }

    /**
     * Create an instance of {@link BookingAmendmentRequestType }
     * 
     */
    public BookingAmendmentRequestType createBookingAmendmentRequestType() {
        return new BookingAmendmentRequestType();
    }

    /**
     * Create an instance of {@link GetTicketRequestType }
     * 
     */
    public GetTicketRequestType createGetTicketRequestType() {
        return new GetTicketRequestType();
    }

    /**
     * Create an instance of {@link GetTicketRequestType.TicketContext }
     * 
     */
    public GetTicketRequestType.TicketContext createGetTicketRequestTypeTicketContext() {
        return new GetTicketRequestType.TicketContext();
    }

    /**
     * Create an instance of {@link GetTicketRequestType.TicketContext.Issuer }
     * 
     */
    public GetTicketRequestType.TicketContext.Issuer createGetTicketRequestTypeTicketContextIssuer() {
        return new GetTicketRequestType.TicketContext.Issuer();
    }

    /**
     * Create an instance of {@link GetOwnAliasListResponseType }
     * 
     */
    public GetOwnAliasListResponseType createGetOwnAliasListResponseType() {
        return new GetOwnAliasListResponseType();
    }

    /**
     * Create an instance of {@link GetOwnAliasListResponseType.AliasList }
     * 
     */
    public GetOwnAliasListResponseType.AliasList createGetOwnAliasListResponseTypeAliasList() {
        return new GetOwnAliasListResponseType.AliasList();
    }

    /**
     * Create an instance of {@link GetTransactionStatusRequestType }
     * 
     */
    public GetTransactionStatusRequestType createGetTransactionStatusRequestType() {
        return new GetTransactionStatusRequestType();
    }

    /**
     * Create an instance of {@link TransactionAuthorizedRequestType }
     * 
     */
    public TransactionAuthorizedRequestType createTransactionAuthorizedRequestType() {
        return new TransactionAuthorizedRequestType();
    }

    /**
     * Create an instance of {@link SendC2CTransactionRequestType }
     * 
     */
    public SendC2CTransactionRequestType createSendC2CTransactionRequestType() {
        return new SendC2CTransactionRequestType();
    }

    /**
     * Create an instance of {@link RegisterTicketRequestType }
     * 
     */
    public RegisterTicketRequestType createRegisterTicketRequestType() {
        return new RegisterTicketRequestType();
    }

    /**
     * Create an instance of {@link RegisterTicketRequestType.TicketContext }
     * 
     */
    public RegisterTicketRequestType.TicketContext createRegisterTicketRequestTypeTicketContext() {
        return new RegisterTicketRequestType.TicketContext();
    }

    /**
     * Create an instance of {@link GetAppDataResponseType }
     * 
     */
    public GetAppDataResponseType createGetAppDataResponseType() {
        return new GetAppDataResponseType();
    }

    /**
     * Create an instance of {@link TransactionAuthorizedResponseType }
     * 
     */
    public TransactionAuthorizedResponseType createTransactionAuthorizedResponseType() {
        return new TransactionAuthorizedResponseType();
    }

    /**
     * Create an instance of {@link GetTransferStatusRequestType }
     * 
     */
    public GetTransferStatusRequestType createGetTransferStatusRequestType() {
        return new GetTransferStatusRequestType();
    }

    /**
     * Create an instance of {@link CorrectTransactionRequestType }
     * 
     */
    public CorrectTransactionRequestType createCorrectTransactionRequestType() {
        return new CorrectTransactionRequestType();
    }

    /**
     * Create an instance of {@link CancelTransactionResponseType }
     * 
     */
    public CancelTransactionResponseType createCancelTransactionResponseType() {
        return new CancelTransactionResponseType();
    }

    /**
     * Create an instance of {@link GetAliasStatusRequestType }
     * 
     */
    public GetAliasStatusRequestType createGetAliasStatusRequestType() {
        return new GetAliasStatusRequestType();
    }

    /**
     * Create an instance of {@link ProcessTransferResponseType }
     * 
     */
    public ProcessTransferResponseType createProcessTransferResponseType() {
        return new ProcessTransferResponseType();
    }

    /**
     * Create an instance of {@link ProcessTransferResponseType.Alias }
     * 
     */
    public ProcessTransferResponseType.Alias createProcessTransferResponseTypeAlias() {
        return new ProcessTransferResponseType.Alias();
    }

    /**
     * Create an instance of {@link GetAppDataRequestType }
     * 
     */
    public GetAppDataRequestType createGetAppDataRequestType() {
        return new GetAppDataRequestType();
    }

    /**
     * Create an instance of {@link GetAliasRequestType }
     * 
     */
    public GetAliasRequestType createGetAliasRequestType() {
        return new GetAliasRequestType();
    }

    /**
     * Create an instance of {@link CorrectTransactionResponseType }
     * 
     */
    public CorrectTransactionResponseType createCorrectTransactionResponseType() {
        return new CorrectTransactionResponseType();
    }

    /**
     * Create an instance of {@link ProcessTransactionResponseType }
     * 
     */
    public ProcessTransactionResponseType createProcessTransactionResponseType() {
        return new ProcessTransactionResponseType();
    }

    /**
     * Create an instance of {@link ProcessTransactionResponseType.Alias }
     * 
     */
    public ProcessTransactionResponseType.Alias createProcessTransactionResponseTypeAlias() {
        return new ProcessTransactionResponseType.Alias();
    }

    /**
     * Create an instance of {@link GetTransactionStatusResponseType }
     * 
     */
    public GetTransactionStatusResponseType createGetTransactionStatusResponseType() {
        return new GetTransactionStatusResponseType();
    }

    /**
     * Create an instance of {@link CancelTransactionRequestType }
     * 
     */
    public CancelTransactionRequestType createCancelTransactionRequestType() {
        return new CancelTransactionRequestType();
    }

    /**
     * Create an instance of {@link ProcessTransactionRequestType }
     * 
     */
    public ProcessTransactionRequestType createProcessTransactionRequestType() {
        return new ProcessTransactionRequestType();
    }

    /**
     * Create an instance of {@link ProcessTransactionRequestType.AliasList }
     * 
     */
    public ProcessTransactionRequestType.AliasList createProcessTransactionRequestTypeAliasList() {
        return new ProcessTransactionRequestType.AliasList();
    }

    /**
     * Create an instance of {@link GetAliasResponseType }
     * 
     */
    public GetAliasResponseType createGetAliasResponseType() {
        return new GetAliasResponseType();
    }

    /**
     * Create an instance of {@link ProcessTransferRequestType }
     * 
     */
    public ProcessTransferRequestType createProcessTransferRequestType() {
        return new ProcessTransferRequestType();
    }

    /**
     * Create an instance of {@link ProcessTransferRequestType.Sender }
     * 
     */
    public ProcessTransferRequestType.Sender createProcessTransferRequestTypeSender() {
        return new ProcessTransferRequestType.Sender();
    }

    /**
     * Create an instance of {@link ProcessTransferRequestType.AliasList }
     * 
     */
    public ProcessTransferRequestType.AliasList createProcessTransferRequestTypeAliasList() {
        return new ProcessTransferRequestType.AliasList();
    }

    /**
     * Create an instance of {@link SendC2CTransactionResponseType }
     * 
     */
    public SendC2CTransactionResponseType createSendC2CTransactionResponseType() {
        return new SendC2CTransactionResponseType();
    }

    /**
     * Create an instance of {@link RegisterAliasRequestType }
     * 
     */
    public RegisterAliasRequestType createRegisterAliasRequestType() {
        return new RegisterAliasRequestType();
    }

    /**
     * Create an instance of {@link GetTransferStatusResponseType }
     * 
     */
    public GetTransferStatusResponseType createGetTransferStatusResponseType() {
        return new GetTransferStatusResponseType();
    }

    /**
     * Create an instance of {@link TransferAuthorizedRequestType }
     * 
     */
    public TransferAuthorizedRequestType createTransferAuthorizedRequestType() {
        return new TransferAuthorizedRequestType();
    }

    /**
     * Create an instance of {@link CancelTicketRequestType }
     * 
     */
    public CancelTicketRequestType createCancelTicketRequestType() {
        return new CancelTicketRequestType();
    }

    /**
     * Create an instance of {@link CancelTicket }
     * 
     */
    public CancelTicket createCancelTicket() {
        return new CancelTicket();
    }

    /**
     * Create an instance of {@link TransferAuthorized }
     * 
     */
    public TransferAuthorized createTransferAuthorized() {
        return new TransferAuthorized();
    }

    /**
     * Create an instance of {@link GetTransferStatusResponse }
     * 
     */
    public GetTransferStatusResponse createGetTransferStatusResponse() {
        return new GetTransferStatusResponse();
    }

    /**
     * Create an instance of {@link UnRegisterAliasResponse }
     * 
     */
    public UnRegisterAliasResponse createUnRegisterAliasResponse() {
        return new UnRegisterAliasResponse();
    }

    /**
     * Create an instance of {@link UnRegisterAliasResponseType }
     * 
     */
    public UnRegisterAliasResponseType createUnRegisterAliasResponseType() {
        return new UnRegisterAliasResponseType();
    }

    /**
     * Create an instance of {@link RegisterAlias }
     * 
     */
    public RegisterAlias createRegisterAlias() {
        return new RegisterAlias();
    }

    /**
     * Create an instance of {@link SendC2CTransactionResponse }
     * 
     */
    public SendC2CTransactionResponse createSendC2CTransactionResponse() {
        return new SendC2CTransactionResponse();
    }

    /**
     * Create an instance of {@link ProcessTransfer }
     * 
     */
    public ProcessTransfer createProcessTransfer() {
        return new ProcessTransfer();
    }

    /**
     * Create an instance of {@link GetAliasResponse }
     * 
     */
    public GetAliasResponse createGetAliasResponse() {
        return new GetAliasResponse();
    }

    /**
     * Create an instance of {@link RegisterApp }
     * 
     */
    public RegisterApp createRegisterApp() {
        return new RegisterApp();
    }

    /**
     * Create an instance of {@link RegisterAppRequestType }
     * 
     */
    public RegisterAppRequestType createRegisterAppRequestType() {
        return new RegisterAppRequestType();
    }

    /**
     * Create an instance of {@link ProcessTransaction }
     * 
     */
    public ProcessTransaction createProcessTransaction() {
        return new ProcessTransaction();
    }

    /**
     * Create an instance of {@link ChangeAppSts }
     * 
     */
    public ChangeAppSts createChangeAppSts() {
        return new ChangeAppSts();
    }

    /**
     * Create an instance of {@link ChangeAppStsRequestType }
     * 
     */
    public ChangeAppStsRequestType createChangeAppStsRequestType() {
        return new ChangeAppStsRequestType();
    }

    /**
     * Create an instance of {@link CheckTicketResponse }
     * 
     */
    public CheckTicketResponse createCheckTicketResponse() {
        return new CheckTicketResponse();
    }

    /**
     * Create an instance of {@link CheckTicketResponseType }
     * 
     */
    public CheckTicketResponseType createCheckTicketResponseType() {
        return new CheckTicketResponseType();
    }

    /**
     * Create an instance of {@link ModifyApp }
     * 
     */
    public ModifyApp createModifyApp() {
        return new ModifyApp();
    }

    /**
     * Create an instance of {@link ModifyAppRequestType }
     * 
     */
    public ModifyAppRequestType createModifyAppRequestType() {
        return new ModifyAppRequestType();
    }

    /**
     * Create an instance of {@link CancelTransaction }
     * 
     */
    public CancelTransaction createCancelTransaction() {
        return new CancelTransaction();
    }

    /**
     * Create an instance of {@link GetTransactionStatusResponse }
     * 
     */
    public GetTransactionStatusResponse createGetTransactionStatusResponse() {
        return new GetTransactionStatusResponse();
    }

    /**
     * Create an instance of {@link ProcessTransactionResponse }
     * 
     */
    public ProcessTransactionResponse createProcessTransactionResponse() {
        return new ProcessTransactionResponse();
    }

    /**
     * Create an instance of {@link CorrectTransactionResponse }
     * 
     */
    public CorrectTransactionResponse createCorrectTransactionResponse() {
        return new CorrectTransactionResponse();
    }

    /**
     * Create an instance of {@link GetAlias }
     * 
     */
    public GetAlias createGetAlias() {
        return new GetAlias();
    }

    /**
     * Create an instance of {@link GetOwnAliasList }
     * 
     */
    public GetOwnAliasList createGetOwnAliasList() {
        return new GetOwnAliasList();
    }

    /**
     * Create an instance of {@link GetOwnAliasListRequestType }
     * 
     */
    public GetOwnAliasListRequestType createGetOwnAliasListRequestType() {
        return new GetOwnAliasListRequestType();
    }

    /**
     * Create an instance of {@link GetAppData }
     * 
     */
    public GetAppData createGetAppData() {
        return new GetAppData();
    }

    /**
     * Create an instance of {@link GetTicketResponse }
     * 
     */
    public GetTicketResponse createGetTicketResponse() {
        return new GetTicketResponse();
    }

    /**
     * Create an instance of {@link GetTicketResponseType }
     * 
     */
    public GetTicketResponseType createGetTicketResponseType() {
        return new GetTicketResponseType();
    }

    /**
     * Create an instance of {@link ModifyAppResponse }
     * 
     */
    public ModifyAppResponse createModifyAppResponse() {
        return new ModifyAppResponse();
    }

    /**
     * Create an instance of {@link ModifyAppResponseType }
     * 
     */
    public ModifyAppResponseType createModifyAppResponseType() {
        return new ModifyAppResponseType();
    }

    /**
     * Create an instance of {@link ProcessTransferResponse }
     * 
     */
    public ProcessTransferResponse createProcessTransferResponse() {
        return new ProcessTransferResponse();
    }

    /**
     * Create an instance of {@link GetAppInfo }
     * 
     */
    public GetAppInfo createGetAppInfo() {
        return new GetAppInfo();
    }

    /**
     * Create an instance of {@link GetAppInfoRequestType }
     * 
     */
    public GetAppInfoRequestType createGetAppInfoRequestType() {
        return new GetAppInfoRequestType();
    }

    /**
     * Create an instance of {@link CheckMSISDN }
     * 
     */
    public CheckMSISDN createCheckMSISDN() {
        return new CheckMSISDN();
    }

    /**
     * Create an instance of {@link CheckMSISDNRequestType }
     * 
     */
    public CheckMSISDNRequestType createCheckMSISDNRequestType() {
        return new CheckMSISDNRequestType();
    }

    /**
     * Create an instance of {@link GetAliasStatus }
     * 
     */
    public GetAliasStatus createGetAliasStatus() {
        return new GetAliasStatus();
    }

    /**
     * Create an instance of {@link CancelTransactionResponse }
     * 
     */
    public CancelTransactionResponse createCancelTransactionResponse() {
        return new CancelTransactionResponse();
    }

    /**
     * Create an instance of {@link UnRegisterAppResponse }
     * 
     */
    public UnRegisterAppResponse createUnRegisterAppResponse() {
        return new UnRegisterAppResponse();
    }

    /**
     * Create an instance of {@link UnRegisterAppResponseType }
     * 
     */
    public UnRegisterAppResponseType createUnRegisterAppResponseType() {
        return new UnRegisterAppResponseType();
    }

    /**
     * Create an instance of {@link CheckTicket }
     * 
     */
    public CheckTicket createCheckTicket() {
        return new CheckTicket();
    }

    /**
     * Create an instance of {@link CheckTicketRequestType }
     * 
     */
    public CheckTicketRequestType createCheckTicketRequestType() {
        return new CheckTicketRequestType();
    }

    /**
     * Create an instance of {@link GetAppInfoResponse }
     * 
     */
    public GetAppInfoResponse createGetAppInfoResponse() {
        return new GetAppInfoResponse();
    }

    /**
     * Create an instance of {@link GetAppInfoResponseType }
     * 
     */
    public GetAppInfoResponseType createGetAppInfoResponseType() {
        return new GetAppInfoResponseType();
    }

    /**
     * Create an instance of {@link UnRegisterApp }
     * 
     */
    public UnRegisterApp createUnRegisterApp() {
        return new UnRegisterApp();
    }

    /**
     * Create an instance of {@link UnRegisterAppRequestType }
     * 
     */
    public UnRegisterAppRequestType createUnRegisterAppRequestType() {
        return new UnRegisterAppRequestType();
    }

    /**
     * Create an instance of {@link UnRegisterAlias }
     * 
     */
    public UnRegisterAlias createUnRegisterAlias() {
        return new UnRegisterAlias();
    }

    /**
     * Create an instance of {@link UnRegisterAliasRequestType }
     * 
     */
    public UnRegisterAliasRequestType createUnRegisterAliasRequestType() {
        return new UnRegisterAliasRequestType();
    }

    /**
     * Create an instance of {@link CorrectTransaction }
     * 
     */
    public CorrectTransaction createCorrectTransaction() {
        return new CorrectTransaction();
    }

    /**
     * Create an instance of {@link GetTransferStatus }
     * 
     */
    public GetTransferStatus createGetTransferStatus() {
        return new GetTransferStatus();
    }

    /**
     * Create an instance of {@link TransactionAuthorizedResponse }
     * 
     */
    public TransactionAuthorizedResponse createTransactionAuthorizedResponse() {
        return new TransactionAuthorizedResponse();
    }

    /**
     * Create an instance of {@link ChangeAppStsResponse }
     * 
     */
    public ChangeAppStsResponse createChangeAppStsResponse() {
        return new ChangeAppStsResponse();
    }

    /**
     * Create an instance of {@link ChangeAppStsResponseType }
     * 
     */
    public ChangeAppStsResponseType createChangeAppStsResponseType() {
        return new ChangeAppStsResponseType();
    }

    /**
     * Create an instance of {@link GetAppDataResponse }
     * 
     */
    public GetAppDataResponse createGetAppDataResponse() {
        return new GetAppDataResponse();
    }

    /**
     * Create an instance of {@link RegisterTicket }
     * 
     */
    public RegisterTicket createRegisterTicket() {
        return new RegisterTicket();
    }

    /**
     * Create an instance of {@link SendC2CTransaction }
     * 
     */
    public SendC2CTransaction createSendC2CTransaction() {
        return new SendC2CTransaction();
    }

    /**
     * Create an instance of {@link GetAliasStatusResponse }
     * 
     */
    public GetAliasStatusResponse createGetAliasStatusResponse() {
        return new GetAliasStatusResponse();
    }

    /**
     * Create an instance of {@link GetAliasStatusResponseType }
     * 
     */
    public GetAliasStatusResponseType createGetAliasStatusResponseType() {
        return new GetAliasStatusResponseType();
    }

    /**
     * Create an instance of {@link TransferAuthorizedResponse }
     * 
     */
    public TransferAuthorizedResponse createTransferAuthorizedResponse() {
        return new TransferAuthorizedResponse();
    }

    /**
     * Create an instance of {@link TransferAuthorizedResponseType }
     * 
     */
    public TransferAuthorizedResponseType createTransferAuthorizedResponseType() {
        return new TransferAuthorizedResponseType();
    }

    /**
     * Create an instance of {@link TransactionAuthorized }
     * 
     */
    public TransactionAuthorized createTransactionAuthorized() {
        return new TransactionAuthorized();
    }

    /**
     * Create an instance of {@link RegisterAliasResponse }
     * 
     */
    public RegisterAliasResponse createRegisterAliasResponse() {
        return new RegisterAliasResponse();
    }

    /**
     * Create an instance of {@link RegisterAliasResponseType }
     * 
     */
    public RegisterAliasResponseType createRegisterAliasResponseType() {
        return new RegisterAliasResponseType();
    }

    /**
     * Create an instance of {@link RegisterTicketResponse }
     * 
     */
    public RegisterTicketResponse createRegisterTicketResponse() {
        return new RegisterTicketResponse();
    }

    /**
     * Create an instance of {@link RegisterTicketResponseType }
     * 
     */
    public RegisterTicketResponseType createRegisterTicketResponseType() {
        return new RegisterTicketResponseType();
    }

    /**
     * Create an instance of {@link GetTransactionStatus }
     * 
     */
    public GetTransactionStatus createGetTransactionStatus() {
        return new GetTransactionStatus();
    }

    /**
     * Create an instance of {@link RegisterAppResponse }
     * 
     */
    public RegisterAppResponse createRegisterAppResponse() {
        return new RegisterAppResponse();
    }

    /**
     * Create an instance of {@link RegisterAppResponseType }
     * 
     */
    public RegisterAppResponseType createRegisterAppResponseType() {
        return new RegisterAppResponseType();
    }

    /**
     * Create an instance of {@link CheckMSISDNResponse }
     * 
     */
    public CheckMSISDNResponse createCheckMSISDNResponse() {
        return new CheckMSISDNResponse();
    }

    /**
     * Create an instance of {@link CheckMSISDNResponseType }
     * 
     */
    public CheckMSISDNResponseType createCheckMSISDNResponseType() {
        return new CheckMSISDNResponseType();
    }

    /**
     * Create an instance of {@link CancelTicketResponse }
     * 
     */
    public CancelTicketResponse createCancelTicketResponse() {
        return new CancelTicketResponse();
    }

    /**
     * Create an instance of {@link CancelTicketResponseType }
     * 
     */
    public CancelTicketResponseType createCancelTicketResponseType() {
        return new CancelTicketResponseType();
    }

    /**
     * Create an instance of {@link GetOwnAliasListResponse }
     * 
     */
    public GetOwnAliasListResponse createGetOwnAliasListResponse() {
        return new GetOwnAliasListResponse();
    }

    /**
     * Create an instance of {@link GetTicket }
     * 
     */
    public GetTicket createGetTicket() {
        return new GetTicket();
    }

    /**
     * Create an instance of {@link BookingAmendmentResponseType }
     * 
     */
    public BookingAmendmentResponseType createBookingAmendmentResponseType() {
        return new BookingAmendmentResponseType();
    }

    /**
     * Create an instance of {@link C2CTransactionBaseType }
     * 
     */
    public C2CTransactionBaseType createC2CTransactionBaseType() {
        return new C2CTransactionBaseType();
    }

    /**
     * Create an instance of {@link IssCorrectTransactionType }
     * 
     */
    public IssCorrectTransactionType createIssCorrectTransactionType() {
        return new IssCorrectTransactionType();
    }

    /**
     * Create an instance of {@link MobDeviceInfoType }
     * 
     */
    public MobDeviceInfoType createMobDeviceInfoType() {
        return new MobDeviceInfoType();
    }

    /**
     * Create an instance of {@link IssuerContextType }
     * 
     */
    public IssuerContextType createIssuerContextType() {
        return new IssuerContextType();
    }

    /**
     * Create an instance of {@link AliasParamType }
     * 
     */
    public AliasParamType createAliasParamType() {
        return new AliasParamType();
    }

    /**
     * Create an instance of {@link MoneyTypeTX }
     * 
     */
    public MoneyTypeTX createMoneyTypeTX() {
        return new MoneyTypeTX();
    }

    /**
     * Create an instance of {@link RevDataType }
     * 
     */
    public RevDataType createRevDataType() {
        return new RevDataType();
    }

    /**
     * Create an instance of {@link QRType }
     * 
     */
    public QRType createQRType() {
        return new QRType();
    }

    /**
     * Create an instance of {@link STLDataType }
     * 
     */
    public STLDataType createSTLDataType() {
        return new STLDataType();
    }

    /**
     * Create an instance of {@link MoneyTypeSTL }
     * 
     */
    public MoneyTypeSTL createMoneyTypeSTL() {
        return new MoneyTypeSTL();
    }

    /**
     * Create an instance of {@link ErrorStatusAttrBlockType }
     * 
     */
    public ErrorStatusAttrBlockType createErrorStatusAttrBlockType() {
        return new ErrorStatusAttrBlockType();
    }

    /**
     * Create an instance of {@link AcqTransactionType }
     * 
     */
    public AcqTransactionType createAcqTransactionType() {
        return new AcqTransactionType();
    }

    /**
     * Create an instance of {@link FinTransferDataType }
     * 
     */
    public FinTransferDataType createFinTransferDataType() {
        return new FinTransferDataType();
    }

    /**
     * Create an instance of {@link AcquirerTicketControlType }
     * 
     */
    public AcquirerTicketControlType createAcquirerTicketControlType() {
        return new AcquirerTicketControlType();
    }

    /**
     * Create an instance of {@link HeaderType }
     * 
     */
    public HeaderType createHeaderType() {
        return new HeaderType();
    }

    /**
     * Create an instance of {@link IssuerTicketControlType }
     * 
     */
    public IssuerTicketControlType createIssuerTicketControlType() {
        return new IssuerTicketControlType();
    }

    /**
     * Create an instance of {@link UserEncrType }
     * 
     */
    public UserEncrType createUserEncrType() {
        return new UserEncrType();
    }

    /**
     * Create an instance of {@link TicketDetailsType }
     * 
     */
    public TicketDetailsType createTicketDetailsType() {
        return new TicketDetailsType();
    }

    /**
     * Create an instance of {@link AliasP2PType }
     * 
     */
    public AliasP2PType createAliasP2PType() {
        return new AliasP2PType();
    }

    /**
     * Create an instance of {@link AcqTransactionDetailsType }
     * 
     */
    public AcqTransactionDetailsType createAcqTransactionDetailsType() {
        return new AcqTransactionDetailsType();
    }

    /**
     * Create an instance of {@link AliasMobpayType }
     * 
     */
    public AliasMobpayType createAliasMobpayType() {
        return new AliasMobpayType();
    }

    /**
     * Create an instance of {@link TicketFullType }
     * 
     */
    public TicketFullType createTicketFullType() {
        return new TicketFullType();
    }

    /**
     * Create an instance of {@link AliasScopeType }
     * 
     */
    public AliasScopeType createAliasScopeType() {
        return new AliasScopeType();
    }

    /**
     * Create an instance of {@link AliasType }
     * 
     */
    public AliasType createAliasType() {
        return new AliasType();
    }

    /**
     * Create an instance of {@link OrdC2CTransactionType }
     * 
     */
    public OrdC2CTransactionType createOrdC2CTransactionType() {
        return new OrdC2CTransactionType();
    }

    /**
     * Create an instance of {@link STLTransferDataType }
     * 
     */
    public STLTransferDataType createSTLTransferDataType() {
        return new STLTransferDataType();
    }

    /**
     * Create an instance of {@link CLSDataType }
     * 
     */
    public CLSDataType createCLSDataType() {
        return new CLSDataType();
    }

    /**
     * Create an instance of {@link AddressType }
     * 
     */
    public AddressType createAddressType() {
        return new AddressType();
    }

    /**
     * Create an instance of {@link FinDataType }
     * 
     */
    public FinDataType createFinDataType() {
        return new FinDataType();
    }

    /**
     * Create an instance of {@link AcqCorrectTransactionType }
     * 
     */
    public AcqCorrectTransactionType createAcqCorrectTransactionType() {
        return new AcqCorrectTransactionType();
    }

    /**
     * Create an instance of {@link AliasMobpayTxType }
     * 
     */
    public AliasMobpayTxType createAliasMobpayTxType() {
        return new AliasMobpayTxType();
    }

    /**
     * Create an instance of {@link BenC2CTransactionType }
     * 
     */
    public BenC2CTransactionType createBenC2CTransactionType() {
        return new BenC2CTransactionType();
    }

    /**
     * Create an instance of {@link AutopaymentType }
     * 
     */
    public AutopaymentType createAutopaymentType() {
        return new AutopaymentType();
    }

    /**
     * Create an instance of {@link ListRequestType }
     * 
     */
    public ListRequestType createListRequestType() {
        return new ListRequestType();
    }

    /**
     * Create an instance of {@link IssTransferDataType }
     * 
     */
    public IssTransferDataType createIssTransferDataType() {
        return new IssTransferDataType();
    }

    /**
     * Create an instance of {@link TXDataType }
     * 
     */
    public TXDataType createTXDataType() {
        return new TXDataType();
    }

    /**
     * Create an instance of {@link MoneyType }
     * 
     */
    public MoneyType createMoneyType() {
        return new MoneyType();
    }

    /**
     * Create an instance of {@link TitleType }
     * 
     */
    public TitleType createTitleType() {
        return new TitleType();
    }

    /**
     * Create an instance of {@link BarCodeType }
     * 
     */
    public BarCodeType createBarCodeType() {
        return new BarCodeType();
    }

    /**
     * Create an instance of {@link RequestType }
     * 
     */
    public RequestType createRequestType() {
        return new RequestType();
    }

    /**
     * Create an instance of {@link IssTransactionType }
     * 
     */
    public IssTransactionType createIssTransactionType() {
        return new IssTransactionType();
    }

    /**
     * Create an instance of {@link StatusBlockType }
     * 
     */
    public StatusBlockType createStatusBlockType() {
        return new StatusBlockType();
    }

    /**
     * Create an instance of {@link IssTXDataType }
     * 
     */
    public IssTXDataType createIssTXDataType() {
        return new IssTXDataType();
    }

    /**
     * Create an instance of {@link ClearingDataType }
     * 
     */
    public ClearingDataType createClearingDataType() {
        return new ClearingDataType();
    }

    /**
     * Create an instance of {@link IssTransferType }
     * 
     */
    public IssTransferType createIssTransferType() {
        return new IssTransferType();
    }

    /**
     * Create an instance of {@link com.ipg.mockingblik.generated.tas.TicketType }
     * 
     */
    public com.ipg.mockingblik.generated.tas.TicketType createTicketType() {
        return new com.ipg.mockingblik.generated.tas.TicketType();
    }

    /**
     * Create an instance of {@link FieldReturnedType }
     * 
     */
    public FieldReturnedType createFieldReturnedType() {
        return new FieldReturnedType();
    }

    /**
     * Create an instance of {@link PinDataType }
     * 
     */
    public PinDataType createPinDataType() {
        return new PinDataType();
    }

    /**
     * Create an instance of {@link TransferDataType }
     * 
     */
    public TransferDataType createTransferDataType() {
        return new TransferDataType();
    }

    /**
     * Create an instance of {@link TransferType }
     * 
     */
    public TransferType createTransferType() {
        return new TransferType();
    }

    /**
     * Create an instance of {@link FieldType }
     * 
     */
    public FieldType createFieldType() {
        return new FieldType();
    }

    /**
     * Create an instance of {@link BenDataEncrType }
     * 
     */
    public BenDataEncrType createBenDataEncrType() {
        return new BenDataEncrType();
    }

    /**
     * Create an instance of {@link IssuerDetailsType }
     * 
     */
    public IssuerDetailsType createIssuerDetailsType() {
        return new IssuerDetailsType();
    }

    /**
     * Create an instance of {@link IbanEncrType }
     * 
     */
    public IbanEncrType createIbanEncrType() {
        return new IbanEncrType();
    }

    /**
     * Create an instance of {@link CorrectTransactionType }
     * 
     */
    public CorrectTransactionType createCorrectTransactionType() {
        return new CorrectTransactionType();
    }

    /**
     * Create an instance of {@link AlternativeType }
     * 
     */
    public AlternativeType createAlternativeType() {
        return new AlternativeType();
    }

    /**
     * Create an instance of {@link AdditionalInfoListType.Item }
     * 
     */
    public AdditionalInfoListType.Item createAdditionalInfoListTypeItem() {
        return new AdditionalInfoListType.Item();
    }

    /**
     * Create an instance of {@link SimpleTransferType.Goods }
     * 
     */
    public SimpleTransferType.Goods createSimpleTransferTypeGoods() {
        return new SimpleTransferType.Goods();
    }

    /**
     * Create an instance of {@link SimpleTransferType.Comment }
     * 
     */
    public SimpleTransferType.Comment createSimpleTransferTypeComment() {
        return new SimpleTransferType.Comment();
    }

    /**
     * Create an instance of {@link SimpleTransferType.AdditionalData }
     * 
     */
    public SimpleTransferType.AdditionalData createSimpleTransferTypeAdditionalData() {
        return new SimpleTransferType.AdditionalData();
    }

    /**
     * Create an instance of {@link ErrorStatusType.Success }
     * 
     */
    public ErrorStatusType.Success createErrorStatusTypeSuccess() {
        return new ErrorStatusType.Success();
    }

    /**
     * Create an instance of {@link ErrorStatusType.Error }
     * 
     */
    public ErrorStatusType.Error createErrorStatusTypeError() {
        return new ErrorStatusType.Error();
    }

    /**
     * Create an instance of {@link SimpleTransactionType.Goods }
     * 
     */
    public SimpleTransactionType.Goods createSimpleTransactionTypeGoods() {
        return new SimpleTransactionType.Goods();
    }

    /**
     * Create an instance of {@link SimpleTransactionType.Comment }
     * 
     */
    public SimpleTransactionType.Comment createSimpleTransactionTypeComment() {
        return new SimpleTransactionType.Comment();
    }

    /**
     * Create an instance of {@link SimpleTransactionType.AdditionalData }
     * 
     */
    public SimpleTransactionType.AdditionalData createSimpleTransactionTypeAdditionalData() {
        return new SimpleTransactionType.AdditionalData();
    }

    /**
     * Create an instance of {@link MerchantTransferDetailsType.Name }
     * 
     */
    public MerchantTransferDetailsType.Name createMerchantTransferDetailsTypeName() {
        return new MerchantTransferDetailsType.Name();
    }

    /**
     * Create an instance of {@link MerchantTransferDetailsType.Device }
     * 
     */
    public MerchantTransferDetailsType.Device createMerchantTransferDetailsTypeDevice() {
        return new MerchantTransferDetailsType.Device();
    }

    /**
     * Create an instance of {@link EventType.Data.Ctx.CtxParam }
     * 
     */
    public EventType.Data.Ctx.CtxParam createEventTypeDataCtxCtxParam() {
        return new EventType.Data.Ctx.CtxParam();
    }

    /**
     * Create an instance of {@link EventType.Data.Key.Alias }
     * 
     */
    public EventType.Data.Key.Alias createEventTypeDataKeyAlias() {
        return new EventType.Data.Key.Alias();
    }

    /**
     * Create an instance of {@link MerchantDetailsType.Name }
     * 
     */
    public MerchantDetailsType.Name createMerchantDetailsTypeName() {
        return new MerchantDetailsType.Name();
    }

    /**
     * Create an instance of {@link MerchantDetailsType.Device }
     * 
     */
    public MerchantDetailsType.Device createMerchantDetailsTypeDevice() {
        return new MerchantDetailsType.Device();
    }

    /**
     * Create an instance of {@link PagingType.PagingItem }
     * 
     */
    public PagingType.PagingItem createPagingTypePagingItem() {
        return new PagingType.PagingItem();
    }

    /**
     * Create an instance of {@link ParametersListType.Parameter }
     * 
     */
    public ParametersListType.Parameter createParametersListTypeParameter() {
        return new ParametersListType.Parameter();
    }

    /**
     * Create an instance of {@link ListResponseType.PagingInfo }
     * 
     */
    public ListResponseType.PagingInfo createListResponseTypePagingInfo() {
        return new ListResponseType.PagingInfo();
    }

    /**
     * Create an instance of {@link BookingAmendmentRequestType.Acquirer }
     * 
     */
    public BookingAmendmentRequestType.Acquirer createBookingAmendmentRequestTypeAcquirer() {
        return new BookingAmendmentRequestType.Acquirer();
    }

    /**
     * Create an instance of {@link BookingAmendmentRequestType.Tas }
     * 
     */
    public BookingAmendmentRequestType.Tas createBookingAmendmentRequestTypeTas() {
        return new BookingAmendmentRequestType.Tas();
    }

    /**
     * Create an instance of {@link BookingAmendmentRequestType.TxData }
     * 
     */
    public BookingAmendmentRequestType.TxData createBookingAmendmentRequestTypeTxData() {
        return new BookingAmendmentRequestType.TxData();
    }

    /**
     * Create an instance of {@link BookingAmendmentRequestType.FinData }
     * 
     */
    public BookingAmendmentRequestType.FinData createBookingAmendmentRequestTypeFinData() {
        return new BookingAmendmentRequestType.FinData();
    }

    /**
     * Create an instance of {@link BookingAmendmentRequestType.StlData }
     * 
     */
    public BookingAmendmentRequestType.StlData createBookingAmendmentRequestTypeStlData() {
        return new BookingAmendmentRequestType.StlData();
    }

    /**
     * Create an instance of {@link GetTicketRequestType.TicketType }
     * 
     */
    public GetTicketRequestType.TicketType createGetTicketRequestTypeTicketType() {
        return new GetTicketRequestType.TicketType();
    }

    /**
     * Create an instance of {@link GetTicketRequestType.TicketContext.Issuer.TicketControl }
     * 
     */
    public GetTicketRequestType.TicketContext.Issuer.TicketControl createGetTicketRequestTypeTicketContextIssuerTicketControl() {
        return new GetTicketRequestType.TicketContext.Issuer.TicketControl();
    }

    /**
     * Create an instance of {@link GetOwnAliasListResponseType.AliasList.Alias }
     * 
     */
    public GetOwnAliasListResponseType.AliasList.Alias createGetOwnAliasListResponseTypeAliasListAlias() {
        return new GetOwnAliasListResponseType.AliasList.Alias();
    }

    /**
     * Create an instance of {@link GetTransactionStatusRequestType.Transaction }
     * 
     */
    public GetTransactionStatusRequestType.Transaction createGetTransactionStatusRequestTypeTransaction() {
        return new GetTransactionStatusRequestType.Transaction();
    }

    /**
     * Create an instance of {@link GetTransactionStatusRequestType.Acquirer }
     * 
     */
    public GetTransactionStatusRequestType.Acquirer createGetTransactionStatusRequestTypeAcquirer() {
        return new GetTransactionStatusRequestType.Acquirer();
    }

    /**
     * Create an instance of {@link GetTransactionStatusRequestType.Issuer }
     * 
     */
    public GetTransactionStatusRequestType.Issuer createGetTransactionStatusRequestTypeIssuer() {
        return new GetTransactionStatusRequestType.Issuer();
    }

    /**
     * Create an instance of {@link TransactionAuthorizedRequestType.Transaction }
     * 
     */
    public TransactionAuthorizedRequestType.Transaction createTransactionAuthorizedRequestTypeTransaction() {
        return new TransactionAuthorizedRequestType.Transaction();
    }

    /**
     * Create an instance of {@link TransactionAuthorizedRequestType.Authorized }
     * 
     */
    public TransactionAuthorizedRequestType.Authorized createTransactionAuthorizedRequestTypeAuthorized() {
        return new TransactionAuthorizedRequestType.Authorized();
    }

    /**
     * Create an instance of {@link TransactionAuthorizedRequestType.Declined }
     * 
     */
    public TransactionAuthorizedRequestType.Declined createTransactionAuthorizedRequestTypeDeclined() {
        return new TransactionAuthorizedRequestType.Declined();
    }

    /**
     * Create an instance of {@link SendC2CTransactionRequestType.Transaction }
     * 
     */
    public SendC2CTransactionRequestType.Transaction createSendC2CTransactionRequestTypeTransaction() {
        return new SendC2CTransactionRequestType.Transaction();
    }

    /**
     * Create an instance of {@link RegisterTicketRequestType.TicketType }
     * 
     */
    public RegisterTicketRequestType.TicketType createRegisterTicketRequestTypeTicketType() {
        return new RegisterTicketRequestType.TicketType();
    }

    /**
     * Create an instance of {@link RegisterTicketRequestType.TicketContext.Issuer }
     * 
     */
    public RegisterTicketRequestType.TicketContext.Issuer createRegisterTicketRequestTypeTicketContextIssuer() {
        return new RegisterTicketRequestType.TicketContext.Issuer();
    }

    /**
     * Create an instance of {@link GetAppDataResponseType.ParamAliasList }
     * 
     */
    public GetAppDataResponseType.ParamAliasList createGetAppDataResponseTypeParamAliasList() {
        return new GetAppDataResponseType.ParamAliasList();
    }

    /**
     * Create an instance of {@link TransactionAuthorizedResponseType.ClsData }
     * 
     */
    public TransactionAuthorizedResponseType.ClsData createTransactionAuthorizedResponseTypeClsData() {
        return new TransactionAuthorizedResponseType.ClsData();
    }

    /**
     * Create an instance of {@link GetTransferStatusRequestType.Transfer }
     * 
     */
    public GetTransferStatusRequestType.Transfer createGetTransferStatusRequestTypeTransfer() {
        return new GetTransferStatusRequestType.Transfer();
    }

    /**
     * Create an instance of {@link GetTransferStatusRequestType.Acquirer }
     * 
     */
    public GetTransferStatusRequestType.Acquirer createGetTransferStatusRequestTypeAcquirer() {
        return new GetTransferStatusRequestType.Acquirer();
    }

    /**
     * Create an instance of {@link GetTransferStatusRequestType.Issuer }
     * 
     */
    public GetTransferStatusRequestType.Issuer createGetTransferStatusRequestTypeIssuer() {
        return new GetTransferStatusRequestType.Issuer();
    }

    /**
     * Create an instance of {@link CorrectTransactionRequestType.Acquirer }
     * 
     */
    public CorrectTransactionRequestType.Acquirer createCorrectTransactionRequestTypeAcquirer() {
        return new CorrectTransactionRequestType.Acquirer();
    }

    /**
     * Create an instance of {@link CorrectTransactionRequestType.Reason }
     * 
     */
    public CorrectTransactionRequestType.Reason createCorrectTransactionRequestTypeReason() {
        return new CorrectTransactionRequestType.Reason();
    }

    /**
     * Create an instance of {@link CancelTransactionResponseType.Transaction }
     * 
     */
    public CancelTransactionResponseType.Transaction createCancelTransactionResponseTypeTransaction() {
        return new CancelTransactionResponseType.Transaction();
    }

    /**
     * Create an instance of {@link CancelTransactionResponseType.ClsData }
     * 
     */
    public CancelTransactionResponseType.ClsData createCancelTransactionResponseTypeClsData() {
        return new CancelTransactionResponseType.ClsData();
    }

    /**
     * Create an instance of {@link GetAliasStatusRequestType.Issuer }
     * 
     */
    public GetAliasStatusRequestType.Issuer createGetAliasStatusRequestTypeIssuer() {
        return new GetAliasStatusRequestType.Issuer();
    }

    /**
     * Create an instance of {@link ProcessTransferResponseType.Transfer }
     * 
     */
    public ProcessTransferResponseType.Transfer createProcessTransferResponseTypeTransfer() {
        return new ProcessTransferResponseType.Transfer();
    }

    /**
     * Create an instance of {@link ProcessTransferResponseType.Alias.Alternatives }
     * 
     */
    public ProcessTransferResponseType.Alias.Alternatives createProcessTransferResponseTypeAliasAlternatives() {
        return new ProcessTransferResponseType.Alias.Alternatives();
    }

    /**
     * Create an instance of {@link GetAppDataRequestType.Acquirer }
     * 
     */
    public GetAppDataRequestType.Acquirer createGetAppDataRequestTypeAcquirer() {
        return new GetAppDataRequestType.Acquirer();
    }

    /**
     * Create an instance of {@link GetAppDataRequestType.ParamAliasList }
     * 
     */
    public GetAppDataRequestType.ParamAliasList createGetAppDataRequestTypeParamAliasList() {
        return new GetAppDataRequestType.ParamAliasList();
    }

    /**
     * Create an instance of {@link GetAliasRequestType.Issuer }
     * 
     */
    public GetAliasRequestType.Issuer createGetAliasRequestTypeIssuer() {
        return new GetAliasRequestType.Issuer();
    }

    /**
     * Create an instance of {@link CorrectTransactionResponseType.Transaction }
     * 
     */
    public CorrectTransactionResponseType.Transaction createCorrectTransactionResponseTypeTransaction() {
        return new CorrectTransactionResponseType.Transaction();
    }

    /**
     * Create an instance of {@link CorrectTransactionResponseType.ClsData }
     * 
     */
    public CorrectTransactionResponseType.ClsData createCorrectTransactionResponseTypeClsData() {
        return new CorrectTransactionResponseType.ClsData();
    }

    /**
     * Create an instance of {@link ProcessTransactionResponseType.Transaction }
     * 
     */
    public ProcessTransactionResponseType.Transaction createProcessTransactionResponseTypeTransaction() {
        return new ProcessTransactionResponseType.Transaction();
    }

    /**
     * Create an instance of {@link ProcessTransactionResponseType.Alias.Alternatives }
     * 
     */
    public ProcessTransactionResponseType.Alias.Alternatives createProcessTransactionResponseTypeAliasAlternatives() {
        return new ProcessTransactionResponseType.Alias.Alternatives();
    }

    /**
     * Create an instance of {@link GetTransactionStatusResponseType.Transaction }
     * 
     */
    public GetTransactionStatusResponseType.Transaction createGetTransactionStatusResponseTypeTransaction() {
        return new GetTransactionStatusResponseType.Transaction();
    }

    /**
     * Create an instance of {@link GetTransactionStatusResponseType.ClsData }
     * 
     */
    public GetTransactionStatusResponseType.ClsData createGetTransactionStatusResponseTypeClsData() {
        return new GetTransactionStatusResponseType.ClsData();
    }

    /**
     * Create an instance of {@link CancelTransactionRequestType.Acquirer }
     * 
     */
    public CancelTransactionRequestType.Acquirer createCancelTransactionRequestTypeAcquirer() {
        return new CancelTransactionRequestType.Acquirer();
    }

    /**
     * Create an instance of {@link CancelTransactionRequestType.Transaction }
     * 
     */
    public CancelTransactionRequestType.Transaction createCancelTransactionRequestTypeTransaction() {
        return new CancelTransactionRequestType.Transaction();
    }

    /**
     * Create an instance of {@link CancelTransactionRequestType.Reason }
     * 
     */
    public CancelTransactionRequestType.Reason createCancelTransactionRequestTypeReason() {
        return new CancelTransactionRequestType.Reason();
    }

    /**
     * Create an instance of {@link ProcessTransactionRequestType.Ticket }
     * 
     */
    public ProcessTransactionRequestType.Ticket createProcessTransactionRequestTypeTicket() {
        return new ProcessTransactionRequestType.Ticket();
    }

    /**
     * Create an instance of {@link ProcessTransactionRequestType.AndAliasList }
     * 
     */
    public ProcessTransactionRequestType.AndAliasList createProcessTransactionRequestTypeAndAliasList() {
        return new ProcessTransactionRequestType.AndAliasList();
    }

    /**
     * Create an instance of {@link ProcessTransactionRequestType.Acquirer }
     * 
     */
    public ProcessTransactionRequestType.Acquirer createProcessTransactionRequestTypeAcquirer() {
        return new ProcessTransactionRequestType.Acquirer();
    }

    /**
     * Create an instance of {@link ProcessTransactionRequestType.AliasList.Alias }
     * 
     */
    public ProcessTransactionRequestType.AliasList.Alias createProcessTransactionRequestTypeAliasListAlias() {
        return new ProcessTransactionRequestType.AliasList.Alias();
    }

    /**
     * Create an instance of {@link GetAliasResponseType.Issuer }
     * 
     */
    public GetAliasResponseType.Issuer createGetAliasResponseTypeIssuer() {
        return new GetAliasResponseType.Issuer();
    }

    /**
     * Create an instance of {@link ProcessTransferRequestType.Ticket }
     * 
     */
    public ProcessTransferRequestType.Ticket createProcessTransferRequestTypeTicket() {
        return new ProcessTransferRequestType.Ticket();
    }

    /**
     * Create an instance of {@link ProcessTransferRequestType.AndAliasList }
     * 
     */
    public ProcessTransferRequestType.AndAliasList createProcessTransferRequestTypeAndAliasList() {
        return new ProcessTransferRequestType.AndAliasList();
    }

    /**
     * Create an instance of {@link ProcessTransferRequestType.Sender.Issuer }
     * 
     */
    public ProcessTransferRequestType.Sender.Issuer createProcessTransferRequestTypeSenderIssuer() {
        return new ProcessTransferRequestType.Sender.Issuer();
    }

    /**
     * Create an instance of {@link ProcessTransferRequestType.Sender.Acquirer }
     * 
     */
    public ProcessTransferRequestType.Sender.Acquirer createProcessTransferRequestTypeSenderAcquirer() {
        return new ProcessTransferRequestType.Sender.Acquirer();
    }

    /**
     * Create an instance of {@link ProcessTransferRequestType.AliasList.Alias }
     * 
     */
    public ProcessTransferRequestType.AliasList.Alias createProcessTransferRequestTypeAliasListAlias() {
        return new ProcessTransferRequestType.AliasList.Alias();
    }

    /**
     * Create an instance of {@link SendC2CTransactionResponseType.Transaction }
     * 
     */
    public SendC2CTransactionResponseType.Transaction createSendC2CTransactionResponseTypeTransaction() {
        return new SendC2CTransactionResponseType.Transaction();
    }

    /**
     * Create an instance of {@link SendC2CTransactionResponseType.ClsData }
     * 
     */
    public SendC2CTransactionResponseType.ClsData createSendC2CTransactionResponseTypeClsData() {
        return new SendC2CTransactionResponseType.ClsData();
    }

    /**
     * Create an instance of {@link RegisterAliasRequestType.Alias }
     * 
     */
    public RegisterAliasRequestType.Alias createRegisterAliasRequestTypeAlias() {
        return new RegisterAliasRequestType.Alias();
    }

    /**
     * Create an instance of {@link GetTransferStatusResponseType.Transfer }
     * 
     */
    public GetTransferStatusResponseType.Transfer createGetTransferStatusResponseTypeTransfer() {
        return new GetTransferStatusResponseType.Transfer();
    }

    /**
     * Create an instance of {@link GetTransferStatusResponseType.Extended }
     * 
     */
    public GetTransferStatusResponseType.Extended createGetTransferStatusResponseTypeExtended() {
        return new GetTransferStatusResponseType.Extended();
    }

    /**
     * Create an instance of {@link GetTransferStatusResponseType.Authorized }
     * 
     */
    public GetTransferStatusResponseType.Authorized createGetTransferStatusResponseTypeAuthorized() {
        return new GetTransferStatusResponseType.Authorized();
    }

    /**
     * Create an instance of {@link GetTransferStatusResponseType.Declined }
     * 
     */
    public GetTransferStatusResponseType.Declined createGetTransferStatusResponseTypeDeclined() {
        return new GetTransferStatusResponseType.Declined();
    }

    /**
     * Create an instance of {@link TransferAuthorizedRequestType.Transfer }
     * 
     */
    public TransferAuthorizedRequestType.Transfer createTransferAuthorizedRequestTypeTransfer() {
        return new TransferAuthorizedRequestType.Transfer();
    }

    /**
     * Create an instance of {@link TransferAuthorizedRequestType.Authorized }
     * 
     */
    public TransferAuthorizedRequestType.Authorized createTransferAuthorizedRequestTypeAuthorized() {
        return new TransferAuthorizedRequestType.Authorized();
    }

    /**
     * Create an instance of {@link TransferAuthorizedRequestType.Declined }
     * 
     */
    public TransferAuthorizedRequestType.Declined createTransferAuthorizedRequestTypeDeclined() {
        return new TransferAuthorizedRequestType.Declined();
    }

    /**
     * Create an instance of {@link CancelTicketRequestType.TicketContext }
     * 
     */
    public CancelTicketRequestType.TicketContext createCancelTicketRequestTypeTicketContext() {
        return new CancelTicketRequestType.TicketContext();
    }

}
