
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis TAS dla ISS do pobrania statusu aliasu P2P
 * 
 * <p>Java class for getAliasStatusRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAliasStatusRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="issuer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="issId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}IssuerIdType" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;choice>
 *           &lt;element name="msisdnDigest" type="{http://www.hp.com/mobicore/xfmf/common}DigestType"/>
 *           &lt;element name="alias" type="{http://www.hp.com/mobicore/xfmf/common}AliasType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAliasStatusRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "issuer",
    "msisdnDigest",
    "alias"
})
public class GetAliasStatusRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected GetAliasStatusRequestType.Issuer issuer;
    protected String msisdnDigest;
    protected AliasType alias;

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link GetAliasStatusRequestType.Issuer }
     *     
     */
    public GetAliasStatusRequestType.Issuer getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetAliasStatusRequestType.Issuer }
     *     
     */
    public void setIssuer(GetAliasStatusRequestType.Issuer value) {
        this.issuer = value;
    }

    /**
     * Gets the value of the msisdnDigest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdnDigest() {
        return msisdnDigest;
    }

    /**
     * Sets the value of the msisdnDigest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdnDigest(String value) {
        this.msisdnDigest = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link AliasType }
     *     
     */
    public AliasType getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link AliasType }
     *     
     */
    public void setAlias(AliasType value) {
        this.alias = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="issId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}IssuerIdType" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Issuer {

        @XmlAttribute(name = "issId", required = true)
        protected long issId;

        /**
         * Gets the value of the issId property.
         * 
         */
        public long getIssId() {
            return issId;
        }

        /**
         * Sets the value of the issId property.
         * 
         */
        public void setIssId(long value) {
            this.issId = value;
        }

    }

}
