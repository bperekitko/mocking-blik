
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis TAS dla ISS do modyfikacji danych aplikacji użytkownika
 * 
 * <p>Java class for changeAppStsRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="changeAppStsRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="issuer" type="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType"/>
 *         &lt;element name="status" type="{http://www.hp.com/mobicore/xfmf/common}IssAppStatusType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeAppStsRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "issuer",
    "status"
})
public class ChangeAppStsRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected IssuerContextType issuer;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected IssAppStatusType status;

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link IssuerContextType }
     *     
     */
    public IssuerContextType getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssuerContextType }
     *     
     */
    public void setIssuer(IssuerContextType value) {
        this.issuer = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link IssAppStatusType }
     *     
     */
    public IssAppStatusType getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssAppStatusType }
     *     
     */
    public void setStatus(IssAppStatusType value) {
        this.status = value;
    }

}
