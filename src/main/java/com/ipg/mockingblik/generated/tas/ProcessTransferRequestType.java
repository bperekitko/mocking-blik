
package com.ipg.mockingblik.generated.tas;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis udostępniony przez TAS dla Wydawcy/Agenta do autoryzacji transakcji przelewowej
 *                 Serwis asynchroniczny.
 *             
 * 
 * <p>Java class for processTransferRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="processTransferRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;sequence>
 *             &lt;element name="ticket">
 *               &lt;complexType>
 *                 &lt;complexContent>
 *                   &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}TicketDetailsType">
 *                   &lt;/extension>
 *                 &lt;/complexContent>
 *               &lt;/complexType>
 *             &lt;/element>
 *             &lt;element name="andAliasList" minOccurs="0">
 *               &lt;complexType>
 *                 &lt;complexContent>
 *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                     &lt;sequence maxOccurs="10">
 *                       &lt;element name="alias" type="{http://www.hp.com/mobicore/xfmf/common}AliasMobpayTxType"/>
 *                     &lt;/sequence>
 *                   &lt;/restriction>
 *                 &lt;/complexContent>
 *               &lt;/complexType>
 *             &lt;/element>
 *           &lt;/sequence>
 *           &lt;element name="aliasList">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence maxOccurs="10">
 *                     &lt;element name="alias">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}AliasMobpayTxType">
 *                             &lt;sequence>
 *                               &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                               &lt;element name="recomendedAuthLevel" type="{http://www.hp.com/mobicore/xfmf/common}RecomemendedAuthLevelType" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/extension>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="aliasP2PR" type="{http://www.hp.com/mobicore/xfmf/common}AliasP2PType"/>
 *         &lt;/choice>
 *         &lt;element name="sender">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="issuer">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="issId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}IssuerIdType" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="acquirer">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="transfer" type="{http://www.hp.com/mobicore/xfmf/common}TransferType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "processTransferRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "ticket",
    "andAliasList",
    "aliasList",
    "aliasP2PR",
    "sender",
    "transfer"
})
public class ProcessTransferRequestType
    extends RequestType
{

    protected ProcessTransferRequestType.Ticket ticket;
    protected ProcessTransferRequestType.AndAliasList andAliasList;
    protected ProcessTransferRequestType.AliasList aliasList;
    protected AliasP2PType aliasP2PR;
    @XmlElement(required = true)
    protected ProcessTransferRequestType.Sender sender;
    @XmlElement(required = true)
    protected TransferType transfer;

    /**
     * Gets the value of the ticket property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessTransferRequestType.Ticket }
     *     
     */
    public ProcessTransferRequestType.Ticket getTicket() {
        return ticket;
    }

    /**
     * Sets the value of the ticket property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessTransferRequestType.Ticket }
     *     
     */
    public void setTicket(ProcessTransferRequestType.Ticket value) {
        this.ticket = value;
    }

    /**
     * Gets the value of the andAliasList property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessTransferRequestType.AndAliasList }
     *     
     */
    public ProcessTransferRequestType.AndAliasList getAndAliasList() {
        return andAliasList;
    }

    /**
     * Sets the value of the andAliasList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessTransferRequestType.AndAliasList }
     *     
     */
    public void setAndAliasList(ProcessTransferRequestType.AndAliasList value) {
        this.andAliasList = value;
    }

    /**
     * Gets the value of the aliasList property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessTransferRequestType.AliasList }
     *     
     */
    public ProcessTransferRequestType.AliasList getAliasList() {
        return aliasList;
    }

    /**
     * Sets the value of the aliasList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessTransferRequestType.AliasList }
     *     
     */
    public void setAliasList(ProcessTransferRequestType.AliasList value) {
        this.aliasList = value;
    }

    /**
     * Gets the value of the aliasP2PR property.
     * 
     * @return
     *     possible object is
     *     {@link AliasP2PType }
     *     
     */
    public AliasP2PType getAliasP2PR() {
        return aliasP2PR;
    }

    /**
     * Sets the value of the aliasP2PR property.
     * 
     * @param value
     *     allowed object is
     *     {@link AliasP2PType }
     *     
     */
    public void setAliasP2PR(AliasP2PType value) {
        this.aliasP2PR = value;
    }

    /**
     * Gets the value of the sender property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessTransferRequestType.Sender }
     *     
     */
    public ProcessTransferRequestType.Sender getSender() {
        return sender;
    }

    /**
     * Sets the value of the sender property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessTransferRequestType.Sender }
     *     
     */
    public void setSender(ProcessTransferRequestType.Sender value) {
        this.sender = value;
    }

    /**
     * Gets the value of the transfer property.
     * 
     * @return
     *     possible object is
     *     {@link TransferType }
     *     
     */
    public TransferType getTransfer() {
        return transfer;
    }

    /**
     * Sets the value of the transfer property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferType }
     *     
     */
    public void setTransfer(TransferType value) {
        this.transfer = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence maxOccurs="10">
     *         &lt;element name="alias">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}AliasMobpayTxType">
     *                 &lt;sequence>
     *                   &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                   &lt;element name="recomendedAuthLevel" type="{http://www.hp.com/mobicore/xfmf/common}RecomemendedAuthLevelType" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "alias"
    })
    public static class AliasList {

        @XmlElement(required = true)
        protected List<ProcessTransferRequestType.AliasList.Alias> alias;

        /**
         * Gets the value of the alias property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the alias property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAlias().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ProcessTransferRequestType.AliasList.Alias }
         * 
         * 
         */
        public List<ProcessTransferRequestType.AliasList.Alias> getAlias() {
            if (alias == null) {
                alias = new ArrayList<ProcessTransferRequestType.AliasList.Alias>();
            }
            return this.alias;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}AliasMobpayTxType">
         *       &lt;sequence>
         *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *         &lt;element name="recomendedAuthLevel" type="{http://www.hp.com/mobicore/xfmf/common}RecomemendedAuthLevelType" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "key",
            "recomendedAuthLevel"
        })
        public static class Alias
            extends AliasMobpayTxType
        {

            protected Long key;
            @XmlSchemaType(name = "string")
            protected RecomemendedAuthLevelType recomendedAuthLevel;

            /**
             * Gets the value of the key property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getKey() {
                return key;
            }

            /**
             * Sets the value of the key property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setKey(Long value) {
                this.key = value;
            }

            /**
             * Gets the value of the recomendedAuthLevel property.
             * 
             * @return
             *     possible object is
             *     {@link RecomemendedAuthLevelType }
             *     
             */
            public RecomemendedAuthLevelType getRecomendedAuthLevel() {
                return recomendedAuthLevel;
            }

            /**
             * Sets the value of the recomendedAuthLevel property.
             * 
             * @param value
             *     allowed object is
             *     {@link RecomemendedAuthLevelType }
             *     
             */
            public void setRecomendedAuthLevel(RecomemendedAuthLevelType value) {
                this.recomendedAuthLevel = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence maxOccurs="10">
     *         &lt;element name="alias" type="{http://www.hp.com/mobicore/xfmf/common}AliasMobpayTxType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "alias"
    })
    public static class AndAliasList {

        @XmlElement(required = true)
        protected List<AliasMobpayTxType> alias;

        /**
         * Gets the value of the alias property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the alias property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAlias().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AliasMobpayTxType }
         * 
         * 
         */
        public List<AliasMobpayTxType> getAlias() {
            if (alias == null) {
                alias = new ArrayList<AliasMobpayTxType>();
            }
            return this.alias;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="issuer">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;attribute name="issId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}IssuerIdType" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="acquirer">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "issuer",
        "acquirer"
    })
    public static class Sender {

        protected ProcessTransferRequestType.Sender.Issuer issuer;
        protected ProcessTransferRequestType.Sender.Acquirer acquirer;

        /**
         * Gets the value of the issuer property.
         * 
         * @return
         *     possible object is
         *     {@link ProcessTransferRequestType.Sender.Issuer }
         *     
         */
        public ProcessTransferRequestType.Sender.Issuer getIssuer() {
            return issuer;
        }

        /**
         * Sets the value of the issuer property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessTransferRequestType.Sender.Issuer }
         *     
         */
        public void setIssuer(ProcessTransferRequestType.Sender.Issuer value) {
            this.issuer = value;
        }

        /**
         * Gets the value of the acquirer property.
         * 
         * @return
         *     possible object is
         *     {@link ProcessTransferRequestType.Sender.Acquirer }
         *     
         */
        public ProcessTransferRequestType.Sender.Acquirer getAcquirer() {
            return acquirer;
        }

        /**
         * Sets the value of the acquirer property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProcessTransferRequestType.Sender.Acquirer }
         *     
         */
        public void setAcquirer(ProcessTransferRequestType.Sender.Acquirer value) {
            this.acquirer = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Acquirer {

            @XmlAttribute(name = "acqId", required = true)
            protected long acqId;

            /**
             * Gets the value of the acqId property.
             * 
             */
            public long getAcqId() {
                return acqId;
            }

            /**
             * Sets the value of the acqId property.
             * 
             */
            public void setAcqId(long value) {
                this.acqId = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;attribute name="issId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}IssuerIdType" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Issuer {

            @XmlAttribute(name = "issId", required = true)
            protected long issId;

            /**
             * Gets the value of the issId property.
             * 
             */
            public long getIssId() {
                return issId;
            }

            /**
             * Sets the value of the issId property.
             * 
             */
            public void setIssId(long value) {
                this.issId = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}TicketDetailsType">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Ticket
        extends TicketDetailsType
    {


    }

}
