
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis TAS do anulowania wygenerowanego Ticketu. Serwis synchroniczny
 * 
 * <p>Java class for cancelTicketRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cancelTicketRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="ticketContext">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="issuer" type="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ticket" type="{http://www.hp.com/mobicore/xfmf/common}TicketType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cancelTicketRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "ticketContext",
    "ticket"
})
public class CancelTicketRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected CancelTicketRequestType.TicketContext ticketContext;
    @XmlElement(required = true)
    protected TicketType ticket;

    /**
     * Gets the value of the ticketContext property.
     * 
     * @return
     *     possible object is
     *     {@link CancelTicketRequestType.TicketContext }
     *     
     */
    public CancelTicketRequestType.TicketContext getTicketContext() {
        return ticketContext;
    }

    /**
     * Sets the value of the ticketContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelTicketRequestType.TicketContext }
     *     
     */
    public void setTicketContext(CancelTicketRequestType.TicketContext value) {
        this.ticketContext = value;
    }

    /**
     * Gets the value of the ticket property.
     * 
     * @return
     *     possible object is
     *     {@link TicketType }
     *     
     */
    public TicketType getTicket() {
        return ticket;
    }

    /**
     * Sets the value of the ticket property.
     * 
     * @param value
     *     allowed object is
     *     {@link TicketType }
     *     
     */
    public void setTicket(TicketType value) {
        this.ticket = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="issuer" type="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "issuer"
    })
    public static class TicketContext {

        @XmlElement(required = true)
        protected IssuerContextType issuer;

        /**
         * Gets the value of the issuer property.
         * 
         * @return
         *     possible object is
         *     {@link IssuerContextType }
         *     
         */
        public IssuerContextType getIssuer() {
            return issuer;
        }

        /**
         * Sets the value of the issuer property.
         * 
         * @param value
         *     allowed object is
         *     {@link IssuerContextType }
         *     
         */
        public void setIssuer(IssuerContextType value) {
            this.issuer = value;
        }

    }

}
