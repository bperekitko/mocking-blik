
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis implementowany przez TAS dla Agentów. Tranakcja może być anulowana przez Agenta lub
 *                 TAS. Odpowiednik reversala pełnego.
 *             
 * 
 * <p>Java class for cancelTransactionRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cancelTransactionRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="acquirer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="transaction">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;choice>
 *                     &lt;element name="orgTxRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
 *                     &lt;element name="orgExtTxRef" type="{http://www.hp.com/mobicore/xfmf/common}ExtTxRefType"/>
 *                   &lt;/choice>
 *                   &lt;element name="txData" type="{http://www.hp.com/mobicore/xfmf/common}TXDataType"/>
 *                   &lt;element name="extTxRef" type="{http://www.hp.com/mobicore/xfmf/common}ExtTxRefType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="reason">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="code" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcqReversalReasonCodeType" />
 *                 &lt;attribute name="message" type="{http://www.hp.com/mobicore/xfmf/common}Text35Type" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cancelTransactionRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "acquirer",
    "transaction",
    "reason"
})
public class CancelTransactionRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected CancelTransactionRequestType.Acquirer acquirer;
    @XmlElement(required = true)
    protected CancelTransactionRequestType.Transaction transaction;
    @XmlElement(required = true)
    protected CancelTransactionRequestType.Reason reason;

    /**
     * Gets the value of the acquirer property.
     * 
     * @return
     *     possible object is
     *     {@link CancelTransactionRequestType.Acquirer }
     *     
     */
    public CancelTransactionRequestType.Acquirer getAcquirer() {
        return acquirer;
    }

    /**
     * Sets the value of the acquirer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelTransactionRequestType.Acquirer }
     *     
     */
    public void setAcquirer(CancelTransactionRequestType.Acquirer value) {
        this.acquirer = value;
    }

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link CancelTransactionRequestType.Transaction }
     *     
     */
    public CancelTransactionRequestType.Transaction getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelTransactionRequestType.Transaction }
     *     
     */
    public void setTransaction(CancelTransactionRequestType.Transaction value) {
        this.transaction = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link CancelTransactionRequestType.Reason }
     *     
     */
    public CancelTransactionRequestType.Reason getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelTransactionRequestType.Reason }
     *     
     */
    public void setReason(CancelTransactionRequestType.Reason value) {
        this.reason = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Acquirer {

        @XmlAttribute(name = "acqId", required = true)
        protected long acqId;

        /**
         * Gets the value of the acqId property.
         * 
         */
        public long getAcqId() {
            return acqId;
        }

        /**
         * Sets the value of the acqId property.
         * 
         */
        public void setAcqId(long value) {
            this.acqId = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="code" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcqReversalReasonCodeType" />
     *       &lt;attribute name="message" type="{http://www.hp.com/mobicore/xfmf/common}Text35Type" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Reason {

        @XmlAttribute(name = "code", required = true)
        protected AcqReversalReasonCodeType code;
        @XmlAttribute(name = "message")
        protected String message;

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link AcqReversalReasonCodeType }
         *     
         */
        public AcqReversalReasonCodeType getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link AcqReversalReasonCodeType }
         *     
         */
        public void setCode(AcqReversalReasonCodeType value) {
            this.code = value;
        }

        /**
         * Gets the value of the message property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMessage() {
            return message;
        }

        /**
         * Sets the value of the message property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMessage(String value) {
            this.message = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;choice>
     *           &lt;element name="orgTxRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
     *           &lt;element name="orgExtTxRef" type="{http://www.hp.com/mobicore/xfmf/common}ExtTxRefType"/>
     *         &lt;/choice>
     *         &lt;element name="txData" type="{http://www.hp.com/mobicore/xfmf/common}TXDataType"/>
     *         &lt;element name="extTxRef" type="{http://www.hp.com/mobicore/xfmf/common}ExtTxRefType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "orgTxRef",
        "orgExtTxRef",
        "txData",
        "extTxRef"
    })
    public static class Transaction {

        protected Long orgTxRef;
        protected String orgExtTxRef;
        @XmlElement(required = true)
        protected TXDataType txData;
        @XmlElement(required = true)
        protected String extTxRef;

        /**
         * Gets the value of the orgTxRef property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getOrgTxRef() {
            return orgTxRef;
        }

        /**
         * Sets the value of the orgTxRef property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setOrgTxRef(Long value) {
            this.orgTxRef = value;
        }

        /**
         * Gets the value of the orgExtTxRef property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrgExtTxRef() {
            return orgExtTxRef;
        }

        /**
         * Sets the value of the orgExtTxRef property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrgExtTxRef(String value) {
            this.orgExtTxRef = value;
        }

        /**
         * Gets the value of the txData property.
         * 
         * @return
         *     possible object is
         *     {@link TXDataType }
         *     
         */
        public TXDataType getTxData() {
            return txData;
        }

        /**
         * Sets the value of the txData property.
         * 
         * @param value
         *     allowed object is
         *     {@link TXDataType }
         *     
         */
        public void setTxData(TXDataType value) {
            this.txData = value;
        }

        /**
         * Gets the value of the extTxRef property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExtTxRef() {
            return extTxRef;
        }

        /**
         * Sets the value of the extTxRef property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExtTxRef(String value) {
            this.extTxRef = value;
        }

    }

}
