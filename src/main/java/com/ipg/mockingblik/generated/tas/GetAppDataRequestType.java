
package com.ipg.mockingblik.generated.tas;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis TAS dla ISS do pobrania parametrów danego aliasu P2P
 * 
 * <p>Java class for getAppDataRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAppDataRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="acquirer">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="issuer" type="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType"/>
 *         &lt;/choice>
 *         &lt;sequence>
 *           &lt;element name="alias" type="{http://www.hp.com/mobicore/xfmf/common}AliasType"/>
 *           &lt;element name="paramAliasList">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence maxOccurs="unbounded">
 *                     &lt;element name="paramAliasKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAppDataRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "acquirer",
    "issuer",
    "alias",
    "paramAliasList"
})
public class GetAppDataRequestType
    extends RequestType
{

    protected GetAppDataRequestType.Acquirer acquirer;
    protected IssuerContextType issuer;
    @XmlElement(required = true)
    protected AliasType alias;
    @XmlElement(required = true)
    protected GetAppDataRequestType.ParamAliasList paramAliasList;

    /**
     * Gets the value of the acquirer property.
     * 
     * @return
     *     possible object is
     *     {@link GetAppDataRequestType.Acquirer }
     *     
     */
    public GetAppDataRequestType.Acquirer getAcquirer() {
        return acquirer;
    }

    /**
     * Sets the value of the acquirer property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetAppDataRequestType.Acquirer }
     *     
     */
    public void setAcquirer(GetAppDataRequestType.Acquirer value) {
        this.acquirer = value;
    }

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link IssuerContextType }
     *     
     */
    public IssuerContextType getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssuerContextType }
     *     
     */
    public void setIssuer(IssuerContextType value) {
        this.issuer = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link AliasType }
     *     
     */
    public AliasType getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link AliasType }
     *     
     */
    public void setAlias(AliasType value) {
        this.alias = value;
    }

    /**
     * Gets the value of the paramAliasList property.
     * 
     * @return
     *     possible object is
     *     {@link GetAppDataRequestType.ParamAliasList }
     *     
     */
    public GetAppDataRequestType.ParamAliasList getParamAliasList() {
        return paramAliasList;
    }

    /**
     * Sets the value of the paramAliasList property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetAppDataRequestType.ParamAliasList }
     *     
     */
    public void setParamAliasList(GetAppDataRequestType.ParamAliasList value) {
        this.paramAliasList = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Acquirer {

        @XmlAttribute(name = "acqId", required = true)
        protected long acqId;

        /**
         * Gets the value of the acqId property.
         * 
         */
        public long getAcqId() {
            return acqId;
        }

        /**
         * Sets the value of the acqId property.
         * 
         */
        public void setAcqId(long value) {
            this.acqId = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence maxOccurs="unbounded">
     *         &lt;element name="paramAliasKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "paramAliasKey"
    })
    public static class ParamAliasList {

        @XmlElement(required = true)
        protected List<String> paramAliasKey;

        /**
         * Gets the value of the paramAliasKey property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the paramAliasKey property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getParamAliasKey().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getParamAliasKey() {
            if (paramAliasKey == null) {
                paramAliasKey = new ArrayList<String>();
            }
            return this.paramAliasKey;
        }

    }

}
