
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Synchroniczna odpowiedź do correctTransaction.
 * 
 * <p>Java class for correctTransactionResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="correctTransactionResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}ResponseType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="transaction">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;sequence>
 *           &lt;element name="clsData">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}CLSDataType">
 *                 &lt;/extension>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "correctTransactionResponseType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "transaction",
    "clsData"
})
public class CorrectTransactionResponseType
    extends ResponseType
{

    protected CorrectTransactionResponseType.Transaction transaction;
    protected CorrectTransactionResponseType.ClsData clsData;

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link CorrectTransactionResponseType.Transaction }
     *     
     */
    public CorrectTransactionResponseType.Transaction getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorrectTransactionResponseType.Transaction }
     *     
     */
    public void setTransaction(CorrectTransactionResponseType.Transaction value) {
        this.transaction = value;
    }

    /**
     * Gets the value of the clsData property.
     * 
     * @return
     *     possible object is
     *     {@link CorrectTransactionResponseType.ClsData }
     *     
     */
    public CorrectTransactionResponseType.ClsData getClsData() {
        return clsData;
    }

    /**
     * Sets the value of the clsData property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorrectTransactionResponseType.ClsData }
     *     
     */
    public void setClsData(CorrectTransactionResponseType.ClsData value) {
        this.clsData = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}CLSDataType">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ClsData
        extends CLSDataType
    {


    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "txRef"
    })
    public static class Transaction {

        protected long txRef;

        /**
         * Gets the value of the txRef property.
         * 
         */
        public long getTxRef() {
            return txRef;
        }

        /**
         * Sets the value of the txRef property.
         * 
         */
        public void setTxRef(long value) {
            this.txRef = value;
        }

    }

}
