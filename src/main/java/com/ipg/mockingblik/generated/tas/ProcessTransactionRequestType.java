
package com.ipg.mockingblik.generated.tas;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis TAS dla Agentów do autoryzacji transakcji. Serwis "asynchroniczny"
 *             
 * 
 * <p>Java class for processTransactionRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="processTransactionRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;sequence>
 *             &lt;element name="ticket">
 *               &lt;complexType>
 *                 &lt;complexContent>
 *                   &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}TicketDetailsType">
 *                   &lt;/extension>
 *                 &lt;/complexContent>
 *               &lt;/complexType>
 *             &lt;/element>
 *             &lt;element name="andAliasList" minOccurs="0">
 *               &lt;complexType>
 *                 &lt;complexContent>
 *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                     &lt;sequence maxOccurs="10">
 *                       &lt;element name="alias" type="{http://www.hp.com/mobicore/xfmf/common}AliasMobpayTxType"/>
 *                     &lt;/sequence>
 *                   &lt;/restriction>
 *                 &lt;/complexContent>
 *               &lt;/complexType>
 *             &lt;/element>
 *           &lt;/sequence>
 *           &lt;element name="orgTxRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
 *           &lt;sequence>
 *             &lt;element name="aliasList">
 *               &lt;complexType>
 *                 &lt;complexContent>
 *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                     &lt;sequence maxOccurs="10">
 *                       &lt;element name="alias">
 *                         &lt;complexType>
 *                           &lt;complexContent>
 *                             &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}AliasMobpayTxType">
 *                               &lt;sequence>
 *                                 &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                                 &lt;element name="recomendedAuthLevel" type="{http://www.hp.com/mobicore/xfmf/common}RecomemendedAuthLevelType" minOccurs="0"/>
 *                               &lt;/sequence>
 *                             &lt;/extension>
 *                           &lt;/complexContent>
 *                         &lt;/complexType>
 *                       &lt;/element>
 *                     &lt;/sequence>
 *                   &lt;/restriction>
 *                 &lt;/complexContent>
 *               &lt;/complexType>
 *             &lt;/element>
 *           &lt;/sequence>
 *         &lt;/choice>
 *         &lt;element name="acquirer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="transaction" type="{http://www.hp.com/mobicore/xfmf/common}AcqTransactionType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "processTransactionRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "ticket",
    "andAliasList",
    "orgTxRef",
    "aliasList",
    "acquirer",
    "transaction"
})
public class ProcessTransactionRequestType
    extends RequestType
{

    protected ProcessTransactionRequestType.Ticket ticket;
    protected ProcessTransactionRequestType.AndAliasList andAliasList;
    protected Long orgTxRef;
    protected ProcessTransactionRequestType.AliasList aliasList;
    @XmlElement(required = true)
    protected ProcessTransactionRequestType.Acquirer acquirer;
    @XmlElement(required = true)
    protected AcqTransactionType transaction;

    /**
     * Gets the value of the ticket property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessTransactionRequestType.Ticket }
     *     
     */
    public ProcessTransactionRequestType.Ticket getTicket() {
        return ticket;
    }

    /**
     * Sets the value of the ticket property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessTransactionRequestType.Ticket }
     *     
     */
    public void setTicket(ProcessTransactionRequestType.Ticket value) {
        this.ticket = value;
    }

    /**
     * Gets the value of the andAliasList property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessTransactionRequestType.AndAliasList }
     *     
     */
    public ProcessTransactionRequestType.AndAliasList getAndAliasList() {
        return andAliasList;
    }

    /**
     * Sets the value of the andAliasList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessTransactionRequestType.AndAliasList }
     *     
     */
    public void setAndAliasList(ProcessTransactionRequestType.AndAliasList value) {
        this.andAliasList = value;
    }

    /**
     * Gets the value of the orgTxRef property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getOrgTxRef() {
        return orgTxRef;
    }

    /**
     * Sets the value of the orgTxRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setOrgTxRef(Long value) {
        this.orgTxRef = value;
    }

    /**
     * Gets the value of the aliasList property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessTransactionRequestType.AliasList }
     *     
     */
    public ProcessTransactionRequestType.AliasList getAliasList() {
        return aliasList;
    }

    /**
     * Sets the value of the aliasList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessTransactionRequestType.AliasList }
     *     
     */
    public void setAliasList(ProcessTransactionRequestType.AliasList value) {
        this.aliasList = value;
    }

    /**
     * Gets the value of the acquirer property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessTransactionRequestType.Acquirer }
     *     
     */
    public ProcessTransactionRequestType.Acquirer getAcquirer() {
        return acquirer;
    }

    /**
     * Sets the value of the acquirer property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessTransactionRequestType.Acquirer }
     *     
     */
    public void setAcquirer(ProcessTransactionRequestType.Acquirer value) {
        this.acquirer = value;
    }

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link AcqTransactionType }
     *     
     */
    public AcqTransactionType getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link AcqTransactionType }
     *     
     */
    public void setTransaction(AcqTransactionType value) {
        this.transaction = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Acquirer {

        @XmlAttribute(name = "acqId", required = true)
        protected long acqId;

        /**
         * Gets the value of the acqId property.
         * 
         */
        public long getAcqId() {
            return acqId;
        }

        /**
         * Sets the value of the acqId property.
         * 
         */
        public void setAcqId(long value) {
            this.acqId = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence maxOccurs="10">
     *         &lt;element name="alias">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}AliasMobpayTxType">
     *                 &lt;sequence>
     *                   &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                   &lt;element name="recomendedAuthLevel" type="{http://www.hp.com/mobicore/xfmf/common}RecomemendedAuthLevelType" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "alias"
    })
    public static class AliasList {

        @XmlElement(required = true)
        protected List<ProcessTransactionRequestType.AliasList.Alias> alias;

        /**
         * Gets the value of the alias property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the alias property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAlias().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ProcessTransactionRequestType.AliasList.Alias }
         * 
         * 
         */
        public List<ProcessTransactionRequestType.AliasList.Alias> getAlias() {
            if (alias == null) {
                alias = new ArrayList<ProcessTransactionRequestType.AliasList.Alias>();
            }
            return this.alias;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}AliasMobpayTxType">
         *       &lt;sequence>
         *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *         &lt;element name="recomendedAuthLevel" type="{http://www.hp.com/mobicore/xfmf/common}RecomemendedAuthLevelType" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "key",
            "recomendedAuthLevel"
        })
        public static class Alias
            extends AliasMobpayTxType
        {

            protected Long key;
            @XmlSchemaType(name = "string")
            protected RecomemendedAuthLevelType recomendedAuthLevel;

            /**
             * Gets the value of the key property.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getKey() {
                return key;
            }

            /**
             * Sets the value of the key property.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setKey(Long value) {
                this.key = value;
            }

            /**
             * Gets the value of the recomendedAuthLevel property.
             * 
             * @return
             *     possible object is
             *     {@link RecomemendedAuthLevelType }
             *     
             */
            public RecomemendedAuthLevelType getRecomendedAuthLevel() {
                return recomendedAuthLevel;
            }

            /**
             * Sets the value of the recomendedAuthLevel property.
             * 
             * @param value
             *     allowed object is
             *     {@link RecomemendedAuthLevelType }
             *     
             */
            public void setRecomendedAuthLevel(RecomemendedAuthLevelType value) {
                this.recomendedAuthLevel = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence maxOccurs="10">
     *         &lt;element name="alias" type="{http://www.hp.com/mobicore/xfmf/common}AliasMobpayTxType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "alias"
    })
    public static class AndAliasList {

        @XmlElement(required = true)
        protected List<AliasMobpayTxType> alias;

        /**
         * Gets the value of the alias property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the alias property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAlias().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AliasMobpayTxType }
         * 
         * 
         */
        public List<AliasMobpayTxType> getAlias() {
            if (alias == null) {
                alias = new ArrayList<AliasMobpayTxType>();
            }
            return this.alias;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}TicketDetailsType">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Ticket
        extends TicketDetailsType
    {


    }

}
