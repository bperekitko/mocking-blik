
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Synchroniczna odpowiedź do getTransactionStatus
 * 
 * <p>Java class for getTransactionStatusResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTransactionStatusResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}ResponseType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="transaction">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="transactionStatus" type="{http://www.hp.com/mobicore/xfmf/common}TasTransactionStatusType"/>
 *         &lt;element name="clsData" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}CLSDataType">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTransactionStatusResponseType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "transaction",
    "transactionStatus",
    "clsData"
})
public class GetTransactionStatusResponseType
    extends ResponseType
{

    protected GetTransactionStatusResponseType.Transaction transaction;
    @XmlSchemaType(name = "string")
    protected TasTransactionStatusType transactionStatus;
    protected GetTransactionStatusResponseType.ClsData clsData;

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link GetTransactionStatusResponseType.Transaction }
     *     
     */
    public GetTransactionStatusResponseType.Transaction getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetTransactionStatusResponseType.Transaction }
     *     
     */
    public void setTransaction(GetTransactionStatusResponseType.Transaction value) {
        this.transaction = value;
    }

    /**
     * Gets the value of the transactionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link TasTransactionStatusType }
     *     
     */
    public TasTransactionStatusType getTransactionStatus() {
        return transactionStatus;
    }

    /**
     * Sets the value of the transactionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link TasTransactionStatusType }
     *     
     */
    public void setTransactionStatus(TasTransactionStatusType value) {
        this.transactionStatus = value;
    }

    /**
     * Gets the value of the clsData property.
     * 
     * @return
     *     possible object is
     *     {@link GetTransactionStatusResponseType.ClsData }
     *     
     */
    public GetTransactionStatusResponseType.ClsData getClsData() {
        return clsData;
    }

    /**
     * Sets the value of the clsData property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetTransactionStatusResponseType.ClsData }
     *     
     */
    public void setClsData(GetTransactionStatusResponseType.ClsData value) {
        this.clsData = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}CLSDataType">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ClsData
        extends CLSDataType
    {


    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "txRef"
    })
    public static class Transaction {

        protected long txRef;

        /**
         * Gets the value of the txRef property.
         * 
         */
        public long getTxRef() {
            return txRef;
        }

        /**
         * Sets the value of the txRef property.
         * 
         */
        public void setTxRef(long value) {
            this.txRef = value;
        }

    }

}
