
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Synchroniczna odpowiedź do getTransferStatus
 * 
 * <p>Java class for getTransferStatusResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTransferStatusResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}ResponseType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="transfer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="transferStatus" type="{http://www.hp.com/mobicore/xfmf/common}TransferStatusType"/>
 *         &lt;element name="extended" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="clearingData" type="{http://www.hp.com/mobicore/xfmf/common}ClearingDataType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="authorized">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="aprovalCode" type="{http://www.hp.com/mobicore/xfmf/common}AprovalCodeType"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="declined">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="reasonCode" type="{http://www.hp.com/mobicore/xfmf/common}AuthDeclineCodeType"/>
 *                     &lt;element name="reasonMessage" type="{http://www.hp.com/mobicore/xfmf/common}Text35Type" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTransferStatusResponseType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "transfer",
    "transferStatus",
    "extended",
    "authorized",
    "declined"
})
public class GetTransferStatusResponseType
    extends ResponseType
{

    protected GetTransferStatusResponseType.Transfer transfer;
    @XmlSchemaType(name = "string")
    protected TransferStatusType transferStatus;
    protected GetTransferStatusResponseType.Extended extended;
    protected GetTransferStatusResponseType.Authorized authorized;
    protected GetTransferStatusResponseType.Declined declined;

    /**
     * Gets the value of the transfer property.
     * 
     * @return
     *     possible object is
     *     {@link GetTransferStatusResponseType.Transfer }
     *     
     */
    public GetTransferStatusResponseType.Transfer getTransfer() {
        return transfer;
    }

    /**
     * Sets the value of the transfer property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetTransferStatusResponseType.Transfer }
     *     
     */
    public void setTransfer(GetTransferStatusResponseType.Transfer value) {
        this.transfer = value;
    }

    /**
     * Gets the value of the transferStatus property.
     * 
     * @return
     *     possible object is
     *     {@link TransferStatusType }
     *     
     */
    public TransferStatusType getTransferStatus() {
        return transferStatus;
    }

    /**
     * Sets the value of the transferStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferStatusType }
     *     
     */
    public void setTransferStatus(TransferStatusType value) {
        this.transferStatus = value;
    }

    /**
     * Gets the value of the extended property.
     * 
     * @return
     *     possible object is
     *     {@link GetTransferStatusResponseType.Extended }
     *     
     */
    public GetTransferStatusResponseType.Extended getExtended() {
        return extended;
    }

    /**
     * Sets the value of the extended property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetTransferStatusResponseType.Extended }
     *     
     */
    public void setExtended(GetTransferStatusResponseType.Extended value) {
        this.extended = value;
    }

    /**
     * Gets the value of the authorized property.
     * 
     * @return
     *     possible object is
     *     {@link GetTransferStatusResponseType.Authorized }
     *     
     */
    public GetTransferStatusResponseType.Authorized getAuthorized() {
        return authorized;
    }

    /**
     * Sets the value of the authorized property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetTransferStatusResponseType.Authorized }
     *     
     */
    public void setAuthorized(GetTransferStatusResponseType.Authorized value) {
        this.authorized = value;
    }

    /**
     * Gets the value of the declined property.
     * 
     * @return
     *     possible object is
     *     {@link GetTransferStatusResponseType.Declined }
     *     
     */
    public GetTransferStatusResponseType.Declined getDeclined() {
        return declined;
    }

    /**
     * Sets the value of the declined property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetTransferStatusResponseType.Declined }
     *     
     */
    public void setDeclined(GetTransferStatusResponseType.Declined value) {
        this.declined = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="aprovalCode" type="{http://www.hp.com/mobicore/xfmf/common}AprovalCodeType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "aprovalCode"
    })
    public static class Authorized {

        @XmlElement(required = true)
        protected String aprovalCode;

        /**
         * Gets the value of the aprovalCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAprovalCode() {
            return aprovalCode;
        }

        /**
         * Sets the value of the aprovalCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAprovalCode(String value) {
            this.aprovalCode = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="reasonCode" type="{http://www.hp.com/mobicore/xfmf/common}AuthDeclineCodeType"/>
     *         &lt;element name="reasonMessage" type="{http://www.hp.com/mobicore/xfmf/common}Text35Type" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reasonCode",
        "reasonMessage"
    })
    public static class Declined {

        @XmlElement(required = true)
        @XmlSchemaType(name = "string")
        protected AuthDeclineCodeType reasonCode;
        protected String reasonMessage;

        /**
         * Gets the value of the reasonCode property.
         * 
         * @return
         *     possible object is
         *     {@link AuthDeclineCodeType }
         *     
         */
        public AuthDeclineCodeType getReasonCode() {
            return reasonCode;
        }

        /**
         * Sets the value of the reasonCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link AuthDeclineCodeType }
         *     
         */
        public void setReasonCode(AuthDeclineCodeType value) {
            this.reasonCode = value;
        }

        /**
         * Gets the value of the reasonMessage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReasonMessage() {
            return reasonMessage;
        }

        /**
         * Sets the value of the reasonMessage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReasonMessage(String value) {
            this.reasonMessage = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="clearingData" type="{http://www.hp.com/mobicore/xfmf/common}ClearingDataType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "clearingData"
    })
    public static class Extended {

        protected ClearingDataType clearingData;

        /**
         * Gets the value of the clearingData property.
         * 
         * @return
         *     possible object is
         *     {@link ClearingDataType }
         *     
         */
        public ClearingDataType getClearingData() {
            return clearingData;
        }

        /**
         * Sets the value of the clearingData property.
         * 
         * @param value
         *     allowed object is
         *     {@link ClearingDataType }
         *     
         */
        public void setClearingData(ClearingDataType value) {
            this.clearingData = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "txRef"
    })
    public static class Transfer {

        protected long txRef;

        /**
         * Gets the value of the txRef property.
         * 
         */
        public long getTxRef() {
            return txRef;
        }

        /**
         * Sets the value of the txRef property.
         * 
         */
        public void setTxRef(long value) {
            this.txRef = value;
        }

    }

}
