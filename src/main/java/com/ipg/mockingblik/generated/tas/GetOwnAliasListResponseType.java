
package com.ipg.mockingblik.generated.tas;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for getOwnAliasListResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getOwnAliasListResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}ResponseType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="aliasList">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="alias" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}AliasType">
 *                           &lt;sequence>
 *                             &lt;element name="url" type="{http://www.hp.com/mobicore/xfmf/common}UrlType" minOccurs="0"/>
 *                             &lt;element name="expiration" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                             &lt;element name="ibanEncr" type="{http://www.hp.com/mobicore/xfmf/common}ibanEncrType" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getOwnAliasListResponseType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "aliasList"
})
public class GetOwnAliasListResponseType
    extends ResponseType
{

    protected GetOwnAliasListResponseType.AliasList aliasList;

    /**
     * Gets the value of the aliasList property.
     * 
     * @return
     *     possible object is
     *     {@link GetOwnAliasListResponseType.AliasList }
     *     
     */
    public GetOwnAliasListResponseType.AliasList getAliasList() {
        return aliasList;
    }

    /**
     * Sets the value of the aliasList property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetOwnAliasListResponseType.AliasList }
     *     
     */
    public void setAliasList(GetOwnAliasListResponseType.AliasList value) {
        this.aliasList = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="alias" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}AliasType">
     *                 &lt;sequence>
     *                   &lt;element name="url" type="{http://www.hp.com/mobicore/xfmf/common}UrlType" minOccurs="0"/>
     *                   &lt;element name="expiration" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *                   &lt;element name="ibanEncr" type="{http://www.hp.com/mobicore/xfmf/common}ibanEncrType" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "alias"
    })
    public static class AliasList {

        protected List<GetOwnAliasListResponseType.AliasList.Alias> alias;

        /**
         * Gets the value of the alias property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the alias property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAlias().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GetOwnAliasListResponseType.AliasList.Alias }
         * 
         * 
         */
        public List<GetOwnAliasListResponseType.AliasList.Alias> getAlias() {
            if (alias == null) {
                alias = new ArrayList<GetOwnAliasListResponseType.AliasList.Alias>();
            }
            return this.alias;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}AliasType">
         *       &lt;sequence>
         *         &lt;element name="url" type="{http://www.hp.com/mobicore/xfmf/common}UrlType" minOccurs="0"/>
         *         &lt;element name="expiration" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
         *         &lt;element name="ibanEncr" type="{http://www.hp.com/mobicore/xfmf/common}ibanEncrType" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "url",
            "expiration",
            "ibanEncr"
        })
        public static class Alias
            extends AliasType
        {

            protected String url;
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar expiration;
            protected IbanEncrType ibanEncr;

            /**
             * Gets the value of the url property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUrl() {
                return url;
            }

            /**
             * Sets the value of the url property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUrl(String value) {
                this.url = value;
            }

            /**
             * Gets the value of the expiration property.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getExpiration() {
                return expiration;
            }

            /**
             * Sets the value of the expiration property.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setExpiration(XMLGregorianCalendar value) {
                this.expiration = value;
            }

            /**
             * Gets the value of the ibanEncr property.
             * 
             * @return
             *     possible object is
             *     {@link IbanEncrType }
             *     
             */
            public IbanEncrType getIbanEncr() {
                return ibanEncr;
            }

            /**
             * Sets the value of the ibanEncr property.
             * 
             * @param value
             *     allowed object is
             *     {@link IbanEncrType }
             *     
             */
            public void setIbanEncr(IbanEncrType value) {
                this.ibanEncr = value;
            }

        }

    }

}
