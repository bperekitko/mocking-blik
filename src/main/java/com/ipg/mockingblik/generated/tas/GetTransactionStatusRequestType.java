
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis TAS zwracający status transakcji.
 * 
 * <p>Java class for getTransactionStatusRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTransactionStatusRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="transaction">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="extTxRef" type="{http://www.hp.com/mobicore/xfmf/common}ExtTxRefType"/>
 *                   &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;choice>
 *           &lt;element name="acquirer">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="issuer">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;attribute name="issId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}IssuerIdType" />
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTransactionStatusRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "transaction",
    "acquirer",
    "issuer"
})
public class GetTransactionStatusRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected GetTransactionStatusRequestType.Transaction transaction;
    protected GetTransactionStatusRequestType.Acquirer acquirer;
    protected GetTransactionStatusRequestType.Issuer issuer;

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link GetTransactionStatusRequestType.Transaction }
     *     
     */
    public GetTransactionStatusRequestType.Transaction getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetTransactionStatusRequestType.Transaction }
     *     
     */
    public void setTransaction(GetTransactionStatusRequestType.Transaction value) {
        this.transaction = value;
    }

    /**
     * Gets the value of the acquirer property.
     * 
     * @return
     *     possible object is
     *     {@link GetTransactionStatusRequestType.Acquirer }
     *     
     */
    public GetTransactionStatusRequestType.Acquirer getAcquirer() {
        return acquirer;
    }

    /**
     * Sets the value of the acquirer property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetTransactionStatusRequestType.Acquirer }
     *     
     */
    public void setAcquirer(GetTransactionStatusRequestType.Acquirer value) {
        this.acquirer = value;
    }

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link GetTransactionStatusRequestType.Issuer }
     *     
     */
    public GetTransactionStatusRequestType.Issuer getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetTransactionStatusRequestType.Issuer }
     *     
     */
    public void setIssuer(GetTransactionStatusRequestType.Issuer value) {
        this.issuer = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Acquirer {

        @XmlAttribute(name = "acqId", required = true)
        protected long acqId;

        /**
         * Gets the value of the acqId property.
         * 
         */
        public long getAcqId() {
            return acqId;
        }

        /**
         * Sets the value of the acqId property.
         * 
         */
        public void setAcqId(long value) {
            this.acqId = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="issId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}IssuerIdType" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Issuer {

        @XmlAttribute(name = "issId", required = true)
        protected long issId;

        /**
         * Gets the value of the issId property.
         * 
         */
        public long getIssId() {
            return issId;
        }

        /**
         * Sets the value of the issId property.
         * 
         */
        public void setIssId(long value) {
            this.issId = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="extTxRef" type="{http://www.hp.com/mobicore/xfmf/common}ExtTxRefType"/>
     *         &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "extTxRef",
        "txRef"
    })
    public static class Transaction {

        protected String extTxRef;
        protected Long txRef;

        /**
         * Gets the value of the extTxRef property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExtTxRef() {
            return extTxRef;
        }

        /**
         * Sets the value of the extTxRef property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExtTxRef(String value) {
            this.extTxRef = value;
        }

        /**
         * Gets the value of the txRef property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getTxRef() {
            return txRef;
        }

        /**
         * Sets the value of the txRef property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setTxRef(Long value) {
            this.txRef = value;
        }

    }

}
