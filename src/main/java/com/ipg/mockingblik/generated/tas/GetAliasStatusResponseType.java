
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getAliasStatusResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAliasStatusResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}ResponseType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="aliasStatus" type="{http://www.hp.com/mobicore/xfmf/common}AliasStatusType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAliasStatusResponseType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "aliasStatus"
})
public class GetAliasStatusResponseType
    extends ResponseType
{

    @XmlSchemaType(name = "string")
    protected AliasStatusType aliasStatus;

    /**
     * Gets the value of the aliasStatus property.
     * 
     * @return
     *     possible object is
     *     {@link AliasStatusType }
     *     
     */
    public AliasStatusType getAliasStatus() {
        return aliasStatus;
    }

    /**
     * Sets the value of the aliasStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link AliasStatusType }
     *     
     */
    public void setAliasStatus(AliasStatusType value) {
        this.aliasStatus = value;
    }

}
