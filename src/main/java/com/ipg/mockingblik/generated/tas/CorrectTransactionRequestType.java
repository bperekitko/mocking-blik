
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis udostępniany przez TAS dla Agentów do korekty transakcji Odpwiednik reversala
 *                 częściowego.
 *             
 * 
 * <p>Java class for correctTransactionRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="correctTransactionRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="acquirer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="transaction" type="{http://www.hp.com/mobicore/xfmf/common}AcqCorrectTransactionType"/>
 *         &lt;element name="reason" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="code" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcqCorrectReasonCodeType" />
 *                 &lt;attribute name="message" type="{http://www.hp.com/mobicore/xfmf/common}Text35Type" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "correctTransactionRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "acquirer",
    "transaction",
    "reason"
})
public class CorrectTransactionRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected CorrectTransactionRequestType.Acquirer acquirer;
    @XmlElement(required = true)
    protected AcqCorrectTransactionType transaction;
    protected CorrectTransactionRequestType.Reason reason;

    /**
     * Gets the value of the acquirer property.
     * 
     * @return
     *     possible object is
     *     {@link CorrectTransactionRequestType.Acquirer }
     *     
     */
    public CorrectTransactionRequestType.Acquirer getAcquirer() {
        return acquirer;
    }

    /**
     * Sets the value of the acquirer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorrectTransactionRequestType.Acquirer }
     *     
     */
    public void setAcquirer(CorrectTransactionRequestType.Acquirer value) {
        this.acquirer = value;
    }

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link AcqCorrectTransactionType }
     *     
     */
    public AcqCorrectTransactionType getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link AcqCorrectTransactionType }
     *     
     */
    public void setTransaction(AcqCorrectTransactionType value) {
        this.transaction = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link CorrectTransactionRequestType.Reason }
     *     
     */
    public CorrectTransactionRequestType.Reason getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorrectTransactionRequestType.Reason }
     *     
     */
    public void setReason(CorrectTransactionRequestType.Reason value) {
        this.reason = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="acqId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Acquirer {

        @XmlAttribute(name = "acqId", required = true)
        protected long acqId;

        /**
         * Gets the value of the acqId property.
         * 
         */
        public long getAcqId() {
            return acqId;
        }

        /**
         * Sets the value of the acqId property.
         * 
         */
        public void setAcqId(long value) {
            this.acqId = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="code" use="required" type="{http://www.hp.com/mobicore/xfmf/common}AcqCorrectReasonCodeType" />
     *       &lt;attribute name="message" type="{http://www.hp.com/mobicore/xfmf/common}Text35Type" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Reason {

        @XmlAttribute(name = "code", required = true)
        protected AcqCorrectReasonCodeType code;
        @XmlAttribute(name = "message")
        protected String message;

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link AcqCorrectReasonCodeType }
         *     
         */
        public AcqCorrectReasonCodeType getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link AcqCorrectReasonCodeType }
         *     
         */
        public void setCode(AcqCorrectReasonCodeType value) {
            this.code = value;
        }

        /**
         * Gets the value of the message property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMessage() {
            return message;
        }

        /**
         * Sets the value of the message property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMessage(String value) {
            this.message = value;
        }

    }

}
