
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Serwis TAS dla ISS do rejestracji aliasu
 * 
 * <p>Java class for registerAliasRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="registerAliasRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="issuer" type="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType"/>
 *         &lt;choice>
 *           &lt;sequence>
 *             &lt;element name="msisdnDigest" type="{http://www.hp.com/mobicore/xfmf/common}DigestType"/>
 *             &lt;element name="ibanEncr" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *           &lt;/sequence>
 *           &lt;element name="alias">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}AliasType">
 *                   &lt;sequence minOccurs="0">
 *                     &lt;element name="url" type="{http://www.hp.com/mobicore/xfmf/common}UrlType" minOccurs="0"/>
 *                     &lt;element name="expiration" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                     &lt;element name="registerMode" type="{http://www.hp.com/mobicore/xfmf/common}AliasRegisterModeType" minOccurs="0"/>
 *                     &lt;element name="ibanEncr" type="{http://www.hp.com/mobicore/xfmf/common}ibanEncrType" minOccurs="0"/>
 *                     &lt;element name="devicePlatform" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/extension>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "registerAliasRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "issuer",
    "msisdnDigest",
    "ibanEncr",
    "alias"
})
public class RegisterAliasRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected IssuerContextType issuer;
    protected String msisdnDigest;
    protected byte[] ibanEncr;
    protected RegisterAliasRequestType.Alias alias;

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link IssuerContextType }
     *     
     */
    public IssuerContextType getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssuerContextType }
     *     
     */
    public void setIssuer(IssuerContextType value) {
        this.issuer = value;
    }

    /**
     * Gets the value of the msisdnDigest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdnDigest() {
        return msisdnDigest;
    }

    /**
     * Sets the value of the msisdnDigest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdnDigest(String value) {
        this.msisdnDigest = value;
    }

    /**
     * Gets the value of the ibanEncr property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getIbanEncr() {
        return ibanEncr;
    }

    /**
     * Sets the value of the ibanEncr property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setIbanEncr(byte[] value) {
        this.ibanEncr = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link RegisterAliasRequestType.Alias }
     *     
     */
    public RegisterAliasRequestType.Alias getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegisterAliasRequestType.Alias }
     *     
     */
    public void setAlias(RegisterAliasRequestType.Alias value) {
        this.alias = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}AliasType">
     *       &lt;sequence minOccurs="0">
     *         &lt;element name="url" type="{http://www.hp.com/mobicore/xfmf/common}UrlType" minOccurs="0"/>
     *         &lt;element name="expiration" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="registerMode" type="{http://www.hp.com/mobicore/xfmf/common}AliasRegisterModeType" minOccurs="0"/>
     *         &lt;element name="ibanEncr" type="{http://www.hp.com/mobicore/xfmf/common}ibanEncrType" minOccurs="0"/>
     *         &lt;element name="devicePlatform" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "url",
        "expiration",
        "registerMode",
        "ibanEncr",
        "devicePlatform"
    })
    public static class Alias
        extends AliasType
    {

        protected String url;
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar expiration;
        @XmlSchemaType(name = "string")
        protected AliasRegisterModeType registerMode;
        protected IbanEncrType ibanEncr;
        protected String devicePlatform;

        /**
         * Gets the value of the url property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUrl() {
            return url;
        }

        /**
         * Sets the value of the url property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUrl(String value) {
            this.url = value;
        }

        /**
         * Gets the value of the expiration property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getExpiration() {
            return expiration;
        }

        /**
         * Sets the value of the expiration property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setExpiration(XMLGregorianCalendar value) {
            this.expiration = value;
        }

        /**
         * Gets the value of the registerMode property.
         * 
         * @return
         *     possible object is
         *     {@link AliasRegisterModeType }
         *     
         */
        public AliasRegisterModeType getRegisterMode() {
            return registerMode;
        }

        /**
         * Sets the value of the registerMode property.
         * 
         * @param value
         *     allowed object is
         *     {@link AliasRegisterModeType }
         *     
         */
        public void setRegisterMode(AliasRegisterModeType value) {
            this.registerMode = value;
        }

        /**
         * Gets the value of the ibanEncr property.
         * 
         * @return
         *     possible object is
         *     {@link IbanEncrType }
         *     
         */
        public IbanEncrType getIbanEncr() {
            return ibanEncr;
        }

        /**
         * Sets the value of the ibanEncr property.
         * 
         * @param value
         *     allowed object is
         *     {@link IbanEncrType }
         *     
         */
        public void setIbanEncr(IbanEncrType value) {
            this.ibanEncr = value;
        }

        /**
         * Gets the value of the devicePlatform property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDevicePlatform() {
            return devicePlatform;
        }

        /**
         * Sets the value of the devicePlatform property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDevicePlatform(String value) {
            this.devicePlatform = value;
        }

    }

}
