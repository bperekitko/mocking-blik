
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis udostępniony przez TAS dla Wydawców ("asynchroniczna" odpowiedź do
 *                 authorizeTransaction)
 *             
 * 
 * <p>Java class for transactionAuthorizedRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transactionAuthorizedRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="issuer" type="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType"/>
 *         &lt;element name="transaction">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
 *                   &lt;element name="extTxRef" type="{http://www.hp.com/mobicore/xfmf/common}ExtTxRefType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;choice>
 *           &lt;element name="authorized">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="aprovalCode" type="{http://www.hp.com/mobicore/xfmf/common}AprovalCodeType"/>
 *                     &lt;element name="mktgData" minOccurs="0">
 *                       &lt;simpleType>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                           &lt;maxLength value="256"/>
 *                         &lt;/restriction>
 *                       &lt;/simpleType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="declined">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="reasonCode" type="{http://www.hp.com/mobicore/xfmf/common}AuthDeclineCodeType"/>
 *                     &lt;element name="reasonMessage" type="{http://www.hp.com/mobicore/xfmf/common}Text35Type" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionAuthorizedRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "issuer",
    "transaction",
    "authorized",
    "declined"
})
public class TransactionAuthorizedRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected IssuerContextType issuer;
    @XmlElement(required = true)
    protected TransactionAuthorizedRequestType.Transaction transaction;
    protected TransactionAuthorizedRequestType.Authorized authorized;
    protected TransactionAuthorizedRequestType.Declined declined;

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link IssuerContextType }
     *     
     */
    public IssuerContextType getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssuerContextType }
     *     
     */
    public void setIssuer(IssuerContextType value) {
        this.issuer = value;
    }

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionAuthorizedRequestType.Transaction }
     *     
     */
    public TransactionAuthorizedRequestType.Transaction getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionAuthorizedRequestType.Transaction }
     *     
     */
    public void setTransaction(TransactionAuthorizedRequestType.Transaction value) {
        this.transaction = value;
    }

    /**
     * Gets the value of the authorized property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionAuthorizedRequestType.Authorized }
     *     
     */
    public TransactionAuthorizedRequestType.Authorized getAuthorized() {
        return authorized;
    }

    /**
     * Sets the value of the authorized property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionAuthorizedRequestType.Authorized }
     *     
     */
    public void setAuthorized(TransactionAuthorizedRequestType.Authorized value) {
        this.authorized = value;
    }

    /**
     * Gets the value of the declined property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionAuthorizedRequestType.Declined }
     *     
     */
    public TransactionAuthorizedRequestType.Declined getDeclined() {
        return declined;
    }

    /**
     * Sets the value of the declined property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionAuthorizedRequestType.Declined }
     *     
     */
    public void setDeclined(TransactionAuthorizedRequestType.Declined value) {
        this.declined = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="aprovalCode" type="{http://www.hp.com/mobicore/xfmf/common}AprovalCodeType"/>
     *         &lt;element name="mktgData" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;maxLength value="256"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "aprovalCode",
        "mktgData"
    })
    public static class Authorized {

        @XmlElement(required = true)
        protected String aprovalCode;
        protected String mktgData;

        /**
         * Gets the value of the aprovalCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAprovalCode() {
            return aprovalCode;
        }

        /**
         * Sets the value of the aprovalCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAprovalCode(String value) {
            this.aprovalCode = value;
        }

        /**
         * Gets the value of the mktgData property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMktgData() {
            return mktgData;
        }

        /**
         * Sets the value of the mktgData property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMktgData(String value) {
            this.mktgData = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="reasonCode" type="{http://www.hp.com/mobicore/xfmf/common}AuthDeclineCodeType"/>
     *         &lt;element name="reasonMessage" type="{http://www.hp.com/mobicore/xfmf/common}Text35Type" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reasonCode",
        "reasonMessage"
    })
    public static class Declined {

        @XmlElement(required = true)
        @XmlSchemaType(name = "string")
        protected AuthDeclineCodeType reasonCode;
        protected String reasonMessage;

        /**
         * Gets the value of the reasonCode property.
         * 
         * @return
         *     possible object is
         *     {@link AuthDeclineCodeType }
         *     
         */
        public AuthDeclineCodeType getReasonCode() {
            return reasonCode;
        }

        /**
         * Sets the value of the reasonCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link AuthDeclineCodeType }
         *     
         */
        public void setReasonCode(AuthDeclineCodeType value) {
            this.reasonCode = value;
        }

        /**
         * Gets the value of the reasonMessage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReasonMessage() {
            return reasonMessage;
        }

        /**
         * Sets the value of the reasonMessage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReasonMessage(String value) {
            this.reasonMessage = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
     *         &lt;element name="extTxRef" type="{http://www.hp.com/mobicore/xfmf/common}ExtTxRefType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "txRef",
        "extTxRef"
    })
    public static class Transaction {

        protected long txRef;
        @XmlElement(required = true)
        protected String extTxRef;

        /**
         * Gets the value of the txRef property.
         * 
         */
        public long getTxRef() {
            return txRef;
        }

        /**
         * Sets the value of the txRef property.
         * 
         */
        public void setTxRef(long value) {
            this.txRef = value;
        }

        /**
         * Gets the value of the extTxRef property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExtTxRef() {
            return extTxRef;
        }

        /**
         * Sets the value of the extTxRef property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExtTxRef(String value) {
            this.extTxRef = value;
        }

    }

}
