
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis udostępniony przez TAS dla Wydawców ("asynchroniczna" odpowiedź do
 *                 authorizeTransfer)
 *             
 * 
 * <p>Java class for transferAuthorizedRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transferAuthorizedRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="issuer" type="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType"/>
 *         &lt;element name="transfer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
 *                   &lt;element name="extTxRef" type="{http://www.hp.com/mobicore/xfmf/common}ExtTxRefType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;choice>
 *           &lt;element name="authorized">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="aprovalCode" type="{http://www.hp.com/mobicore/xfmf/common}AprovalCodeType"/>
 *                     &lt;element name="clsMethod" type="{http://www.hp.com/mobicore/xfmf/common}ClsMethodType"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="declined">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="reasonCode" type="{http://www.hp.com/mobicore/xfmf/common}AuthDeclineCodeType"/>
 *                     &lt;element name="reasonMessage" type="{http://www.hp.com/mobicore/xfmf/common}Text35Type" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferAuthorizedRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "issuer",
    "transfer",
    "authorized",
    "declined"
})
public class TransferAuthorizedRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected IssuerContextType issuer;
    @XmlElement(required = true)
    protected TransferAuthorizedRequestType.Transfer transfer;
    protected TransferAuthorizedRequestType.Authorized authorized;
    protected TransferAuthorizedRequestType.Declined declined;

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link IssuerContextType }
     *     
     */
    public IssuerContextType getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssuerContextType }
     *     
     */
    public void setIssuer(IssuerContextType value) {
        this.issuer = value;
    }

    /**
     * Gets the value of the transfer property.
     * 
     * @return
     *     possible object is
     *     {@link TransferAuthorizedRequestType.Transfer }
     *     
     */
    public TransferAuthorizedRequestType.Transfer getTransfer() {
        return transfer;
    }

    /**
     * Sets the value of the transfer property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferAuthorizedRequestType.Transfer }
     *     
     */
    public void setTransfer(TransferAuthorizedRequestType.Transfer value) {
        this.transfer = value;
    }

    /**
     * Gets the value of the authorized property.
     * 
     * @return
     *     possible object is
     *     {@link TransferAuthorizedRequestType.Authorized }
     *     
     */
    public TransferAuthorizedRequestType.Authorized getAuthorized() {
        return authorized;
    }

    /**
     * Sets the value of the authorized property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferAuthorizedRequestType.Authorized }
     *     
     */
    public void setAuthorized(TransferAuthorizedRequestType.Authorized value) {
        this.authorized = value;
    }

    /**
     * Gets the value of the declined property.
     * 
     * @return
     *     possible object is
     *     {@link TransferAuthorizedRequestType.Declined }
     *     
     */
    public TransferAuthorizedRequestType.Declined getDeclined() {
        return declined;
    }

    /**
     * Sets the value of the declined property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferAuthorizedRequestType.Declined }
     *     
     */
    public void setDeclined(TransferAuthorizedRequestType.Declined value) {
        this.declined = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="aprovalCode" type="{http://www.hp.com/mobicore/xfmf/common}AprovalCodeType"/>
     *         &lt;element name="clsMethod" type="{http://www.hp.com/mobicore/xfmf/common}ClsMethodType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "aprovalCode",
        "clsMethod"
    })
    public static class Authorized {

        @XmlElement(required = true)
        protected String aprovalCode;
        @XmlElement(required = true)
        @XmlSchemaType(name = "string")
        protected ClsMethodType clsMethod;

        /**
         * Gets the value of the aprovalCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAprovalCode() {
            return aprovalCode;
        }

        /**
         * Sets the value of the aprovalCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAprovalCode(String value) {
            this.aprovalCode = value;
        }

        /**
         * Gets the value of the clsMethod property.
         * 
         * @return
         *     possible object is
         *     {@link ClsMethodType }
         *     
         */
        public ClsMethodType getClsMethod() {
            return clsMethod;
        }

        /**
         * Sets the value of the clsMethod property.
         * 
         * @param value
         *     allowed object is
         *     {@link ClsMethodType }
         *     
         */
        public void setClsMethod(ClsMethodType value) {
            this.clsMethod = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="reasonCode" type="{http://www.hp.com/mobicore/xfmf/common}AuthDeclineCodeType"/>
     *         &lt;element name="reasonMessage" type="{http://www.hp.com/mobicore/xfmf/common}Text35Type" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reasonCode",
        "reasonMessage"
    })
    public static class Declined {

        @XmlElement(required = true)
        @XmlSchemaType(name = "string")
        protected AuthDeclineCodeType reasonCode;
        protected String reasonMessage;

        /**
         * Gets the value of the reasonCode property.
         * 
         * @return
         *     possible object is
         *     {@link AuthDeclineCodeType }
         *     
         */
        public AuthDeclineCodeType getReasonCode() {
            return reasonCode;
        }

        /**
         * Sets the value of the reasonCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link AuthDeclineCodeType }
         *     
         */
        public void setReasonCode(AuthDeclineCodeType value) {
            this.reasonCode = value;
        }

        /**
         * Gets the value of the reasonMessage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReasonMessage() {
            return reasonMessage;
        }

        /**
         * Sets the value of the reasonMessage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReasonMessage(String value) {
            this.reasonMessage = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
     *         &lt;element name="extTxRef" type="{http://www.hp.com/mobicore/xfmf/common}ExtTxRefType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "txRef",
        "extTxRef"
    })
    public static class Transfer {

        protected long txRef;
        @XmlElement(required = true)
        protected String extTxRef;

        /**
         * Gets the value of the txRef property.
         * 
         */
        public long getTxRef() {
            return txRef;
        }

        /**
         * Sets the value of the txRef property.
         * 
         */
        public void setTxRef(long value) {
            this.txRef = value;
        }

        /**
         * Gets the value of the extTxRef property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExtTxRef() {
            return extTxRef;
        }

        /**
         * Sets the value of the extTxRef property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExtTxRef(String value) {
            this.extTxRef = value;
        }

    }

}
