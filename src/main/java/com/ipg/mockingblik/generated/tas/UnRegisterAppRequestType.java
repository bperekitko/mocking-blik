
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis TAS dla ISS do wyrejestrowania aplikacji użytkownika
 * 
 * <p>Java class for unRegisterAppRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="unRegisterAppRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="issuer" type="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "unRegisterAppRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "issuer"
})
public class UnRegisterAppRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected IssuerContextType issuer;

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link IssuerContextType }
     *     
     */
    public IssuerContextType getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssuerContextType }
     *     
     */
    public void setIssuer(IssuerContextType value) {
        this.issuer = value;
    }

}
