
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis TAS dla ISS do rejestracji aplikacji użytkownika
 * 
 * <p>Java class for registerAppRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="registerAppRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="issuer" type="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType"/>
 *         &lt;choice>
 *           &lt;element name="msisdnDigest" type="{http://www.hp.com/mobicore/xfmf/common}DigestType" minOccurs="0"/>
 *           &lt;element name="app" type="{http://www.hp.com/mobicore/xfmf/common}MobDeviceInfoType"/>
 *         &lt;/choice>
 *         &lt;element name="autoActivateFlg" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "registerAppRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "issuer",
    "msisdnDigest",
    "app",
    "autoActivateFlg"
})
public class RegisterAppRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected IssuerContextType issuer;
    protected String msisdnDigest;
    protected MobDeviceInfoType app;
    @XmlElement(defaultValue = "true")
    protected boolean autoActivateFlg;

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link IssuerContextType }
     *     
     */
    public IssuerContextType getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssuerContextType }
     *     
     */
    public void setIssuer(IssuerContextType value) {
        this.issuer = value;
    }

    /**
     * Gets the value of the msisdnDigest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdnDigest() {
        return msisdnDigest;
    }

    /**
     * Sets the value of the msisdnDigest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdnDigest(String value) {
        this.msisdnDigest = value;
    }

    /**
     * Gets the value of the app property.
     * 
     * @return
     *     possible object is
     *     {@link MobDeviceInfoType }
     *     
     */
    public MobDeviceInfoType getApp() {
        return app;
    }

    /**
     * Sets the value of the app property.
     * 
     * @param value
     *     allowed object is
     *     {@link MobDeviceInfoType }
     *     
     */
    public void setApp(MobDeviceInfoType value) {
        this.app = value;
    }

    /**
     * Gets the value of the autoActivateFlg property.
     * 
     */
    public boolean isAutoActivateFlg() {
        return autoActivateFlg;
    }

    /**
     * Sets the value of the autoActivateFlg property.
     * 
     */
    public void setAutoActivateFlg(boolean value) {
        this.autoActivateFlg = value;
    }

}
