
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Synchroniczna odpowiedź
 * 
 * <p>Java class for getAppInfoResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAppInfoResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}ResponseType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="issuer" type="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType"/>
 *         &lt;choice>
 *           &lt;element name="msisdnDigest" type="{http://www.hp.com/mobicore/xfmf/common}DigestType" minOccurs="0"/>
 *           &lt;element name="app" type="{http://www.hp.com/mobicore/xfmf/common}MobDeviceInfoType"/>
 *         &lt;/choice>
 *         &lt;element name="appStatus" type="{http://www.hp.com/mobicore/xfmf/common}IssAppStatusType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAppInfoResponseType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "issuer",
    "msisdnDigest",
    "app",
    "appStatus"
})
public class GetAppInfoResponseType
    extends ResponseType
{

    protected IssuerContextType issuer;
    protected String msisdnDigest;
    protected MobDeviceInfoType app;
    @XmlSchemaType(name = "string")
    protected IssAppStatusType appStatus;

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link IssuerContextType }
     *     
     */
    public IssuerContextType getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssuerContextType }
     *     
     */
    public void setIssuer(IssuerContextType value) {
        this.issuer = value;
    }

    /**
     * Gets the value of the msisdnDigest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdnDigest() {
        return msisdnDigest;
    }

    /**
     * Sets the value of the msisdnDigest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdnDigest(String value) {
        this.msisdnDigest = value;
    }

    /**
     * Gets the value of the app property.
     * 
     * @return
     *     possible object is
     *     {@link MobDeviceInfoType }
     *     
     */
    public MobDeviceInfoType getApp() {
        return app;
    }

    /**
     * Sets the value of the app property.
     * 
     * @param value
     *     allowed object is
     *     {@link MobDeviceInfoType }
     *     
     */
    public void setApp(MobDeviceInfoType value) {
        this.app = value;
    }

    /**
     * Gets the value of the appStatus property.
     * 
     * @return
     *     possible object is
     *     {@link IssAppStatusType }
     *     
     */
    public IssAppStatusType getAppStatus() {
        return appStatus;
    }

    /**
     * Sets the value of the appStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssAppStatusType }
     *     
     */
    public void setAppStatus(IssAppStatusType value) {
        this.appStatus = value;
    }

}
