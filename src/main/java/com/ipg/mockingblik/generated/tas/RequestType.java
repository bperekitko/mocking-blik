
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}BodyType">
 *       &lt;sequence>
 *         &lt;element name="header" type="{http://www.hp.com/mobicore/xfmf/common}HeaderType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestType", propOrder = {
    "header"
})
@XmlSeeAlso({
    CancelTicketRequestType.class,
    TransferAuthorizedRequestType.class,
    RegisterAliasRequestType.class,
    ProcessTransferRequestType.class,
    RegisterAppRequestType.class,
    ProcessTransactionRequestType.class,
    ChangeAppStsRequestType.class,
    ModifyAppRequestType.class,
    CancelTransactionRequestType.class,
    GetAliasRequestType.class,
    GetOwnAliasListRequestType.class,
    GetAppDataRequestType.class,
    GetAppInfoRequestType.class,
    CheckMSISDNRequestType.class,
    GetAliasStatusRequestType.class,
    CheckTicketRequestType.class,
    UnRegisterAppRequestType.class,
    UnRegisterAliasRequestType.class,
    CorrectTransactionRequestType.class,
    GetTransferStatusRequestType.class,
    RegisterTicketRequestType.class,
    SendC2CTransactionRequestType.class,
    TransactionAuthorizedRequestType.class,
    GetTransactionStatusRequestType.class,
    GetTicketRequestType.class,
    BookingAmendmentRequestType.class,
    ListRequestType.class
})
public class RequestType
    extends BodyType
{

    @XmlElement(required = true)
    protected HeaderType header;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderType }
     *     
     */
    public HeaderType getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderType }
     *     
     */
    public void setHeader(HeaderType value) {
        this.header = value;
    }

}
