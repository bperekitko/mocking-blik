
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis TAS do rejestracji Ticketu dla ISS. Serwis synchroniczny
 * 
 * <p>Java class for registerTicketRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="registerTicketRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="ticketContext">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="issuer">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType">
 *                           &lt;sequence minOccurs="0">
 *                             &lt;element name="ticketControl" type="{http://www.hp.com/mobicore/xfmf/common}IssuerTicketControlType"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ticketType">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}TicketType">
 *                 &lt;sequence>
 *                   &lt;element name="acqId" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "registerTicketRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "ticketContext",
    "ticketType"
})
public class RegisterTicketRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected RegisterTicketRequestType.TicketContext ticketContext;
    @XmlElement(required = true)
    protected RegisterTicketRequestType.TicketType ticketType;

    /**
     * Gets the value of the ticketContext property.
     * 
     * @return
     *     possible object is
     *     {@link RegisterTicketRequestType.TicketContext }
     *     
     */
    public RegisterTicketRequestType.TicketContext getTicketContext() {
        return ticketContext;
    }

    /**
     * Sets the value of the ticketContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegisterTicketRequestType.TicketContext }
     *     
     */
    public void setTicketContext(RegisterTicketRequestType.TicketContext value) {
        this.ticketContext = value;
    }

    /**
     * Gets the value of the ticketType property.
     * 
     * @return
     *     possible object is
     *     {@link RegisterTicketRequestType.TicketType }
     *     
     */
    public RegisterTicketRequestType.TicketType getTicketType() {
        return ticketType;
    }

    /**
     * Sets the value of the ticketType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegisterTicketRequestType.TicketType }
     *     
     */
    public void setTicketType(RegisterTicketRequestType.TicketType value) {
        this.ticketType = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="issuer">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType">
     *                 &lt;sequence minOccurs="0">
     *                   &lt;element name="ticketControl" type="{http://www.hp.com/mobicore/xfmf/common}IssuerTicketControlType"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "issuer"
    })
    public static class TicketContext {

        @XmlElement(required = true)
        protected RegisterTicketRequestType.TicketContext.Issuer issuer;

        /**
         * Gets the value of the issuer property.
         * 
         * @return
         *     possible object is
         *     {@link RegisterTicketRequestType.TicketContext.Issuer }
         *     
         */
        public RegisterTicketRequestType.TicketContext.Issuer getIssuer() {
            return issuer;
        }

        /**
         * Sets the value of the issuer property.
         * 
         * @param value
         *     allowed object is
         *     {@link RegisterTicketRequestType.TicketContext.Issuer }
         *     
         */
        public void setIssuer(RegisterTicketRequestType.TicketContext.Issuer value) {
            this.issuer = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType">
         *       &lt;sequence minOccurs="0">
         *         &lt;element name="ticketControl" type="{http://www.hp.com/mobicore/xfmf/common}IssuerTicketControlType"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ticketControl"
        })
        public static class Issuer
            extends IssuerContextType
        {

            protected IssuerTicketControlType ticketControl;

            /**
             * Gets the value of the ticketControl property.
             * 
             * @return
             *     possible object is
             *     {@link IssuerTicketControlType }
             *     
             */
            public IssuerTicketControlType getTicketControl() {
                return ticketControl;
            }

            /**
             * Sets the value of the ticketControl property.
             * 
             * @param value
             *     allowed object is
             *     {@link IssuerTicketControlType }
             *     
             */
            public void setTicketControl(IssuerTicketControlType value) {
                this.ticketControl = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}TicketType">
     *       &lt;sequence>
     *         &lt;element name="acqId" type="{http://www.hp.com/mobicore/xfmf/common}AcquirerIdType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acqId"
    })
    public static class TicketType
        extends com.ipg.mockingblik.generated.tas.TicketType
    {

        protected Long acqId;

        /**
         * Gets the value of the acqId property.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getAcqId() {
            return acqId;
        }

        /**
         * Sets the value of the acqId property.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setAcqId(Long value) {
            this.acqId = value;
        }

    }

}
