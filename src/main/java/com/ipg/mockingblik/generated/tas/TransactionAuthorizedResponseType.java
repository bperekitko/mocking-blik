
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Synchroniczna odpowiedź do transactionAuthorized
 * 
 * <p>Java class for transactionAuthorizedResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transactionAuthorizedResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}ResponseType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="clsData">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}CLSDataType">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionAuthorizedResponseType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "clsData"
})
public class TransactionAuthorizedResponseType
    extends ResponseType
{

    protected TransactionAuthorizedResponseType.ClsData clsData;

    /**
     * Gets the value of the clsData property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionAuthorizedResponseType.ClsData }
     *     
     */
    public TransactionAuthorizedResponseType.ClsData getClsData() {
        return clsData;
    }

    /**
     * Sets the value of the clsData property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionAuthorizedResponseType.ClsData }
     *     
     */
    public void setClsData(TransactionAuthorizedResponseType.ClsData value) {
        this.clsData = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}CLSDataType">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ClsData
        extends CLSDataType
    {


    }

}
