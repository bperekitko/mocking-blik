
package com.ipg.mockingblik.generated.tas;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "Admin", targetNamespace = "http://www.hp.com/mobicore/services/tas/v1r3")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface Admin {


    /**
     * 
     * @param request
     * @return
     *     returns com.ipg.mockingblik.generated.tas.RegisterAppResponseType
     */
    @WebMethod
    @WebResult(name = "response", targetNamespace = "")
    @RequestWrapper(localName = "registerApp", targetNamespace = "http://www.hp.com/mobicore/services/tas/v1r3", className = "com.ipg.mockingblik.generated.tas.RegisterApp")
    @ResponseWrapper(localName = "registerAppResponse", targetNamespace = "http://www.hp.com/mobicore/services/tas/v1r3", className = "com.ipg.mockingblik.generated.tas.RegisterAppResponse")
    public RegisterAppResponseType registerApp(
        @WebParam(name = "request", targetNamespace = "")
        RegisterAppRequestType request);

    /**
     * 
     * @param request
     * @return
     *     returns com.ipg.mockingblik.generated.tas.UnRegisterAppResponseType
     */
    @WebMethod
    @WebResult(name = "response", targetNamespace = "")
    @RequestWrapper(localName = "unRegisterApp", targetNamespace = "http://www.hp.com/mobicore/services/tas/v1r3", className = "com.ipg.mockingblik.generated.tas.UnRegisterApp")
    @ResponseWrapper(localName = "unRegisterAppResponse", targetNamespace = "http://www.hp.com/mobicore/services/tas/v1r3", className = "com.ipg.mockingblik.generated.tas.UnRegisterAppResponse")
    public UnRegisterAppResponseType unRegisterApp(
        @WebParam(name = "request", targetNamespace = "")
        UnRegisterAppRequestType request);

    /**
     * 
     * @param request
     * @return
     *     returns com.ipg.mockingblik.generated.tas.ModifyAppResponseType
     */
    @WebMethod
    @WebResult(name = "response", targetNamespace = "")
    @RequestWrapper(localName = "modifyApp", targetNamespace = "http://www.hp.com/mobicore/services/tas/v1r3", className = "com.ipg.mockingblik.generated.tas.ModifyApp")
    @ResponseWrapper(localName = "modifyAppResponse", targetNamespace = "http://www.hp.com/mobicore/services/tas/v1r3", className = "com.ipg.mockingblik.generated.tas.ModifyAppResponse")
    public ModifyAppResponseType modifyApp(
        @WebParam(name = "request", targetNamespace = "")
        ModifyAppRequestType request);

    /**
     * 
     * @param request
     * @return
     *     returns com.ipg.mockingblik.generated.tas.ChangeAppStsResponseType
     */
    @WebMethod
    @WebResult(name = "response", targetNamespace = "")
    @RequestWrapper(localName = "changeAppSts", targetNamespace = "http://www.hp.com/mobicore/services/tas/v1r3", className = "com.ipg.mockingblik.generated.tas.ChangeAppSts")
    @ResponseWrapper(localName = "changeAppStsResponse", targetNamespace = "http://www.hp.com/mobicore/services/tas/v1r3", className = "com.ipg.mockingblik.generated.tas.ChangeAppStsResponse")
    public ChangeAppStsResponseType changeAppSts(
        @WebParam(name = "request", targetNamespace = "")
        ChangeAppStsRequestType request);

    /**
     * 
     * @param request
     * @return
     *     returns com.ipg.mockingblik.generated.tas.GetAppInfoResponseType
     */
    @WebMethod
    @WebResult(name = "response", targetNamespace = "")
    @RequestWrapper(localName = "getAppInfo", targetNamespace = "http://www.hp.com/mobicore/services/tas/v1r3", className = "com.ipg.mockingblik.generated.tas.GetAppInfo")
    @ResponseWrapper(localName = "getAppInfoResponse", targetNamespace = "http://www.hp.com/mobicore/services/tas/v1r3", className = "com.ipg.mockingblik.generated.tas.GetAppInfoResponse")
    public GetAppInfoResponseType getAppInfo(
        @WebParam(name = "request", targetNamespace = "")
        GetAppInfoRequestType request);

}
