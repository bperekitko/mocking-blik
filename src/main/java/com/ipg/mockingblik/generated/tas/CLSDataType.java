
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CLSDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CLSDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="sessionNr" use="required">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}long">
 *             &lt;totalDigits value="10"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CLSDataType")
@XmlSeeAlso({
    com.ipg.mockingblik.generated.tas.SendC2CTransactionResponseType.ClsData.class,
    com.ipg.mockingblik.generated.tas.GetTransactionStatusResponseType.ClsData.class,
    com.ipg.mockingblik.generated.tas.CorrectTransactionResponseType.ClsData.class,
    com.ipg.mockingblik.generated.tas.CancelTransactionResponseType.ClsData.class,
    com.ipg.mockingblik.generated.tas.TransactionAuthorizedResponseType.ClsData.class
})
public class CLSDataType {

    @XmlAttribute(name = "sessionNr", required = true)
    protected long sessionNr;

    /**
     * Gets the value of the sessionNr property.
     * 
     */
    public long getSessionNr() {
        return sessionNr;
    }

    /**
     * Sets the value of the sessionNr property.
     * 
     */
    public void setSessionNr(long value) {
        this.sessionNr = value;
    }

}
