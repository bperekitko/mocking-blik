
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis TAS dla ISS do pobrania listy aliasów dla danej aplikacji mobilnej lub danych
 *                 konkretnego aliasu
 *             
 * 
 * <p>Java class for getOwnAliasListRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getOwnAliasListRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="issuer" type="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType"/>
 *         &lt;element name="alias" type="{http://www.hp.com/mobicore/xfmf/common}AliasType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getOwnAliasListRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "issuer",
    "alias"
})
public class GetOwnAliasListRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected IssuerContextType issuer;
    protected AliasType alias;

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link IssuerContextType }
     *     
     */
    public IssuerContextType getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssuerContextType }
     *     
     */
    public void setIssuer(IssuerContextType value) {
        this.issuer = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link AliasType }
     *     
     */
    public AliasType getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link AliasType }
     *     
     */
    public void setAlias(AliasType value) {
        this.alias = value;
    }

}
