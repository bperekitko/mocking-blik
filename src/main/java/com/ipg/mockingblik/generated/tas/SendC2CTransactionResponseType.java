
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sendC2CTransactionResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sendC2CTransactionResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}ResponseType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="transaction">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="clsData">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}CLSDataType">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendC2CTransactionResponseType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "transaction",
    "clsData"
})
public class SendC2CTransactionResponseType
    extends ResponseType
{

    protected SendC2CTransactionResponseType.Transaction transaction;
    protected SendC2CTransactionResponseType.ClsData clsData;

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link SendC2CTransactionResponseType.Transaction }
     *     
     */
    public SendC2CTransactionResponseType.Transaction getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendC2CTransactionResponseType.Transaction }
     *     
     */
    public void setTransaction(SendC2CTransactionResponseType.Transaction value) {
        this.transaction = value;
    }

    /**
     * Gets the value of the clsData property.
     * 
     * @return
     *     possible object is
     *     {@link SendC2CTransactionResponseType.ClsData }
     *     
     */
    public SendC2CTransactionResponseType.ClsData getClsData() {
        return clsData;
    }

    /**
     * Sets the value of the clsData property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendC2CTransactionResponseType.ClsData }
     *     
     */
    public void setClsData(SendC2CTransactionResponseType.ClsData value) {
        this.clsData = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}CLSDataType">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ClsData
        extends CLSDataType
    {


    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="txRef" type="{http://www.hp.com/mobicore/xfmf/common}TxRefType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "txRef"
    })
    public static class Transaction {

        protected long txRef;

        /**
         * Gets the value of the txRef property.
         * 
         */
        public long getTxRef() {
            return txRef;
        }

        /**
         * Sets the value of the txRef property.
         * 
         */
        public void setTxRef(long value) {
            this.txRef = value;
        }

    }

}
