
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis TAS generujący Ticket dla ISS. Serwis synchroniczny
 * 
 * <p>Java class for getTicketRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getTicketRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="ticketContext">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="issuer">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType">
 *                           &lt;sequence minOccurs="0">
 *                             &lt;element name="ticketControl">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}IssuerTicketControlType">
 *                                   &lt;/extension>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ticketType">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="type" use="required">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;maxLength value="20"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTicketRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "ticketContext",
    "ticketType"
})
public class GetTicketRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected GetTicketRequestType.TicketContext ticketContext;
    @XmlElement(required = true)
    protected GetTicketRequestType.TicketType ticketType;

    /**
     * Gets the value of the ticketContext property.
     * 
     * @return
     *     possible object is
     *     {@link GetTicketRequestType.TicketContext }
     *     
     */
    public GetTicketRequestType.TicketContext getTicketContext() {
        return ticketContext;
    }

    /**
     * Sets the value of the ticketContext property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetTicketRequestType.TicketContext }
     *     
     */
    public void setTicketContext(GetTicketRequestType.TicketContext value) {
        this.ticketContext = value;
    }

    /**
     * Gets the value of the ticketType property.
     * 
     * @return
     *     possible object is
     *     {@link GetTicketRequestType.TicketType }
     *     
     */
    public GetTicketRequestType.TicketType getTicketType() {
        return ticketType;
    }

    /**
     * Sets the value of the ticketType property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetTicketRequestType.TicketType }
     *     
     */
    public void setTicketType(GetTicketRequestType.TicketType value) {
        this.ticketType = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="issuer">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType">
     *                 &lt;sequence minOccurs="0">
     *                   &lt;element name="ticketControl">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}IssuerTicketControlType">
     *                         &lt;/extension>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "issuer"
    })
    public static class TicketContext {

        @XmlElement(required = true)
        protected GetTicketRequestType.TicketContext.Issuer issuer;

        /**
         * Gets the value of the issuer property.
         * 
         * @return
         *     possible object is
         *     {@link GetTicketRequestType.TicketContext.Issuer }
         *     
         */
        public GetTicketRequestType.TicketContext.Issuer getIssuer() {
            return issuer;
        }

        /**
         * Sets the value of the issuer property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetTicketRequestType.TicketContext.Issuer }
         *     
         */
        public void setIssuer(GetTicketRequestType.TicketContext.Issuer value) {
            this.issuer = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType">
         *       &lt;sequence minOccurs="0">
         *         &lt;element name="ticketControl">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}IssuerTicketControlType">
         *               &lt;/extension>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ticketControl"
        })
        public static class Issuer
            extends IssuerContextType
        {

            protected GetTicketRequestType.TicketContext.Issuer.TicketControl ticketControl;

            /**
             * Gets the value of the ticketControl property.
             * 
             * @return
             *     possible object is
             *     {@link GetTicketRequestType.TicketContext.Issuer.TicketControl }
             *     
             */
            public GetTicketRequestType.TicketContext.Issuer.TicketControl getTicketControl() {
                return ticketControl;
            }

            /**
             * Sets the value of the ticketControl property.
             * 
             * @param value
             *     allowed object is
             *     {@link GetTicketRequestType.TicketContext.Issuer.TicketControl }
             *     
             */
            public void setTicketControl(GetTicketRequestType.TicketContext.Issuer.TicketControl value) {
                this.ticketControl = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}IssuerTicketControlType">
             *     &lt;/extension>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class TicketControl
                extends IssuerTicketControlType
            {


            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="type" use="required">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;maxLength value="20"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TicketType {

        @XmlAttribute(name = "type", required = true)
        protected String type;

        /**
         * Gets the value of the type property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Sets the value of the type property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

    }

}
