
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Serwis udostępniony przez TAS dla Wydawcy do autoryzacji transakcji C2C.Serwis
 *                 synchroniczny.
 *             
 * 
 * <p>Java class for sendC2CTransactionRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sendC2CTransactionRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}RequestType">
 *       &lt;sequence>
 *         &lt;element name="transaction">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}OrdC2CTransactionType">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="issuer" type="{http://www.hp.com/mobicore/xfmf/common}IssuerContextType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendC2CTransactionRequestType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "transaction",
    "issuer"
})
public class SendC2CTransactionRequestType
    extends RequestType
{

    @XmlElement(required = true)
    protected SendC2CTransactionRequestType.Transaction transaction;
    @XmlElement(required = true)
    protected IssuerContextType issuer;

    /**
     * Gets the value of the transaction property.
     * 
     * @return
     *     possible object is
     *     {@link SendC2CTransactionRequestType.Transaction }
     *     
     */
    public SendC2CTransactionRequestType.Transaction getTransaction() {
        return transaction;
    }

    /**
     * Sets the value of the transaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendC2CTransactionRequestType.Transaction }
     *     
     */
    public void setTransaction(SendC2CTransactionRequestType.Transaction value) {
        this.transaction = value;
    }

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link IssuerContextType }
     *     
     */
    public IssuerContextType getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link IssuerContextType }
     *     
     */
    public void setIssuer(IssuerContextType value) {
        this.issuer = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}OrdC2CTransactionType">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Transaction
        extends OrdC2CTransactionType
    {


    }

}
