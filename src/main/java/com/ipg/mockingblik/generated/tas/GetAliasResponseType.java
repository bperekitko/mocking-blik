
package com.ipg.mockingblik.generated.tas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getAliasResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getAliasResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.hp.com/mobicore/xfmf/common}ResponseType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="issuer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="issId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}IssuerIdType" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="msisdnDigest" type="{http://www.hp.com/mobicore/xfmf/common}DigestType"/>
 *         &lt;element name="ibanEncr" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAliasResponseType", namespace = "http://www.hp.com/mobicore/xfmf/tas", propOrder = {
    "issuer",
    "msisdnDigest",
    "ibanEncr"
})
public class GetAliasResponseType
    extends ResponseType
{

    protected GetAliasResponseType.Issuer issuer;
    protected String msisdnDigest;
    protected byte[] ibanEncr;

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link GetAliasResponseType.Issuer }
     *     
     */
    public GetAliasResponseType.Issuer getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetAliasResponseType.Issuer }
     *     
     */
    public void setIssuer(GetAliasResponseType.Issuer value) {
        this.issuer = value;
    }

    /**
     * Gets the value of the msisdnDigest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsisdnDigest() {
        return msisdnDigest;
    }

    /**
     * Sets the value of the msisdnDigest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsisdnDigest(String value) {
        this.msisdnDigest = value;
    }

    /**
     * Gets the value of the ibanEncr property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getIbanEncr() {
        return ibanEncr;
    }

    /**
     * Sets the value of the ibanEncr property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setIbanEncr(byte[] value) {
        this.ibanEncr = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="issId" use="required" type="{http://www.hp.com/mobicore/xfmf/common}IssuerIdType" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Issuer {

        @XmlAttribute(name = "issId", required = true)
        protected long issId;

        /**
         * Gets the value of the issId property.
         * 
         */
        public long getIssId() {
            return issId;
        }

        /**
         * Sets the value of the issId property.
         * 
         */
        public void setIssId(long value) {
            this.issId = value;
        }

    }

}
