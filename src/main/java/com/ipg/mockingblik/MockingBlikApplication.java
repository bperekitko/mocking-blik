package com.ipg.mockingblik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.ipg"})
public class MockingBlikApplication {

	public static void main(String[] args) {
		SpringApplication.run(MockingBlikApplication.class, args);
	}
}
