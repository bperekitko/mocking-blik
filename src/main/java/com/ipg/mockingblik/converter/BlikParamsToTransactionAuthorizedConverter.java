package com.ipg.mockingblik.converter;

import com.ipg.mockingblik.generated.tas.AuthDeclineCodeType;
import com.ipg.mockingblik.generated.tas.TransactionAuthorized;
import com.ipg.mockingblik.generated.tas.TransactionAuthorizedRequestType;
import com.ipg.mockingblik.generated.tas.TransactionAuthorizedRequestType.Authorized;
import com.ipg.mockingblik.generated.tas.TransactionAuthorizedRequestType.Declined;
import com.ipg.mockingblik.generated.tas.TransactionAuthorizedRequestType.Transaction;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Random;

@Component
public class BlikParamsToTransactionAuthorizedConverter implements Converter<Map<String, String>, TransactionAuthorized> {

    @Override
    public TransactionAuthorized convert(@Nullable Map<String, String> source) {
        TransactionAuthorized transactionAuthorized = new TransactionAuthorized();
        transactionAuthorized.setRequest(buildRequest(source));
        return transactionAuthorized;
    }

    private TransactionAuthorizedRequestType buildRequest(Map<String, String> source) {
        String description = source.get("Description");
        String extTxId = source.get("CustomParam");
        return "ERROR".equalsIgnoreCase(description) ? declinedResponse(extTxId) : authorizedResponse(extTxId);
    }

    private TransactionAuthorizedRequestType authorizedResponse(String extTxId) {
        TransactionAuthorizedRequestType type = new TransactionAuthorizedRequestType();
        type.setTransaction(buildTransaction(extTxId));
        type.setAuthorized(buildAuthorized());
        return type;
    }

    private Transaction buildTransaction(String extTxId) {
        Transaction transaction = new Transaction();
        transaction.setExtTxRef(extTxId);
        transaction.setTxRef(new Random().nextInt(10000000));
        return transaction;
    }

    private Authorized buildAuthorized() {
        Authorized authorized = new Authorized();
        authorized.setAprovalCode("APPR1");
        authorized.setMktgData("Mocking BLIK MarketingData");
        return authorized;
    }

    private TransactionAuthorizedRequestType declinedResponse(String extTxId) {
        TransactionAuthorizedRequestType type = new TransactionAuthorizedRequestType();
        type.setTransaction(buildTransaction(extTxId));
        type.setDeclined(buildDeclined());
        return type;
    }

    private Declined buildDeclined() {
        Declined declined = new Declined();
        declined.setReasonCode(randomCode());
        declined.setReasonMessage("Mocking BLIK reason");
        return declined;
    }

    private AuthDeclineCodeType randomCode() {
        AuthDeclineCodeType[] values = AuthDeclineCodeType.values();
        return values[new Random().nextInt(values.length)];
    }
}
