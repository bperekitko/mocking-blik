package com.ipg.mockingblik.response;

import com.ipg.api.blik.model.response.BlikResponse;
import com.ipg.api.blik.signature.BlikSignatureBuilder;
import com.ipg.mockingblik.generated.tas.TransactionAuthorized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Random;

@Component
public class DirectResponseSender {
    private static final String SERVICE_URL =
            "http://localhost:8181/camel/engine/services/paymentnotification?paysolId=BLIK";

    private final ConversionService conversionService;
    private final RestTemplate restTemplate;
    private final BlikSignatureBuilder signatureBuilder;

    @Autowired
    public DirectResponseSender(ConversionService conversionService, BlikSignatureBuilder signatureBuilder) {
        this.conversionService = conversionService;
        this.signatureBuilder = signatureBuilder;
        this.restTemplate = new RestTemplate();
    }

    public void sensSoapResponse(Map<String, String> params) {
        String message = buildTransactionSoapMessage(params);
        HttpEntity<String> entity = new HttpEntity<>(message);
        restTemplate.postForEntity(SERVICE_URL, entity, String.class);
    }

    private String buildTransactionSoapMessage(Map<String, String> params) {
        return marshall(conversionService.convert(params, TransactionAuthorized.class));
    }

    private String marshall(TransactionAuthorized transaction) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            SOAPMessage message = buildSoapMessage(transaction);
            message.writeTo(outputStream);

            return new String(outputStream.toByteArray());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private SOAPMessage buildSoapMessage(TransactionAuthorized transaction) throws JAXBException,
            ParserConfigurationException, SOAPException {
        Document document = buildSoapDocument(transaction);

        SOAPMessage message = MessageFactory.newInstance().createMessage();
        message.getSOAPBody().addDocument(document);
        message.getSOAPHeader().detachNode();
        return message;
    }

    private Document buildSoapDocument(TransactionAuthorized transaction)
            throws ParserConfigurationException, JAXBException {
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

        JAXBContext jaxbContext = JAXBContext.newInstance(TransactionAuthorized.class);
        jaxbContext.createMarshaller().marshal(transaction, document);

        return document;
    }

    public void sendRestResponse(Map<String, String> paramsMap) {
        HttpHeaders headers = new HttpHeaders();
        MediaType media = new MediaType(MediaType.APPLICATION_FORM_URLENCODED, Charset.forName("ISO-8859-2"));
        headers.setContentType(media);

        MultiValueMap<String, String> body = buildBlikTransactionUrlEncoded(paramsMap);
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(body, headers);

        restTemplate.postForEntity(SERVICE_URL, entity, String.class);
    }

    private MultiValueMap<String, String> buildBlikTransactionUrlEncoded(Map<String, String> paramsMap) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        paramsMap.forEach(map::set);
        map.set("TxID", String.valueOf(new Random().nextInt(10000000)));
        map.set("ControlData", calculateControlData(map.toSingleValueMap()));
        return map;
    }

    private String calculateControlData(Map<String, String> paramsMap) {
        BlikResponse blikResponse = BlikResponse.parseFromMap(paramsMap);
        return signatureBuilder.buildSignature(blikResponse);
    }
}
